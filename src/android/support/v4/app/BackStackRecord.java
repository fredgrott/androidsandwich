/*
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package android.support.v4.app;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.Log;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.ArrayList;

// TODO: Auto-generated Javadoc
/**
 * 
 * @author fredgrott
 *
 */
final class BackStackState implements Parcelable {
    
    /** The m ops. */
    final int[] mOps;
    
    /** The m transition. */
    final int mTransition;
    
    /** The m transition style. */
    final int mTransitionStyle;
    
    /** The m name. */
    final String mName;
    
    /** The m index. */
    final int mIndex;
    
    /** The m bread crumb title res. */
    final int mBreadCrumbTitleRes;
    
    /** The m bread crumb title text. */
    final CharSequence mBreadCrumbTitleText;
    
    /** The m bread crumb short title res. */
    final int mBreadCrumbShortTitleRes;
    
    /** The m bread crumb short title text. */
    final CharSequence mBreadCrumbShortTitleText;

    /**
     * Instantiates a new back stack state.
     *
     * @param fm the fm
     * @param bse the bse
     */
    public BackStackState(FragmentManagerImpl fm, BackStackRecord bse) {
        int numRemoved = 0;
        BackStackRecord.Op op = bse.mHead;
        while (op != null) {
            if (op.removed != null) {
				numRemoved += op.removed.size();
			}
            op = op.next;
        }
        mOps = new int[bse.mNumOp * 7 + numRemoved];

        if (!bse.mAddToBackStack) {
            throw new IllegalStateException("Not on back stack");
        }

        op = bse.mHead;
        int pos = 0;
        while (op != null) {
            mOps[pos++] = op.cmd;
            mOps[pos++] = op.fragment.mIndex;
            mOps[pos++] = op.enterAnim;
            mOps[pos++] = op.exitAnim;
            mOps[pos++] = op.popEnterAnim;
            mOps[pos++] = op.popExitAnim;
            if (op.removed != null) {
                final int N = op.removed.size();
                mOps[pos++] = N;
                for (int i = 0; i < N; i++) {
                    mOps[pos++] = op.removed.get(i).mIndex;
                }
            } else {
                mOps[pos++] = 0;
            }
            op = op.next;
        }
        mTransition = bse.mTransition;
        mTransitionStyle = bse.mTransitionStyle;
        mName = bse.mName;
        mIndex = bse.mIndex;
        mBreadCrumbTitleRes = bse.mBreadCrumbTitleRes;
        mBreadCrumbTitleText = bse.mBreadCrumbTitleText;
        mBreadCrumbShortTitleRes = bse.mBreadCrumbShortTitleRes;
        mBreadCrumbShortTitleText = bse.mBreadCrumbShortTitleText;
    }

    /**
     * Instantiates a new back stack state.
     *
     * @param in the in
     */
    public BackStackState(Parcel in) {
        mOps = in.createIntArray();
        mTransition = in.readInt();
        mTransitionStyle = in.readInt();
        mName = in.readString();
        mIndex = in.readInt();
        mBreadCrumbTitleRes = in.readInt();
        mBreadCrumbTitleText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
        mBreadCrumbShortTitleRes = in.readInt();
        mBreadCrumbShortTitleText = TextUtils.CHAR_SEQUENCE_CREATOR.createFromParcel(in);
    }

    /**
     * Instantiate.
     *
     * @param fm the fm
     * @return the back stack record
     */
    public BackStackRecord instantiate(FragmentManagerImpl fm) {
        BackStackRecord bse = new BackStackRecord(fm);
        int pos = 0;
        while (pos < mOps.length) {
            BackStackRecord.Op op = new BackStackRecord.Op();
            op.cmd = mOps[pos++];
            if (FragmentManagerImpl.DEBUG) {
				Log.v(FragmentManagerImpl.TAG,
				        "BSE " + bse + " set base fragment #" + mOps[pos]);
			}
            Fragment f = fm.mActive.get(mOps[pos++]);
            op.fragment = f;
            op.enterAnim = mOps[pos++];
            op.exitAnim = mOps[pos++];
            op.popEnterAnim = mOps[pos++];
            op.popExitAnim = mOps[pos++];
            final int N = mOps[pos++];
            if (N > 0) {
                op.removed = new ArrayList<Fragment>(N);
                for (int i = 0; i < N; i++) {
                    if (FragmentManagerImpl.DEBUG) {
						Log.v(FragmentManagerImpl.TAG,
						        "BSE " + bse + " set remove fragment #" + mOps[pos]);
					}
                    Fragment r = fm.mActive.get(mOps[pos++]);
                    op.removed.add(r);
                }
            }
            bse.addOp(op);
        }
        bse.mTransition = mTransition;
        bse.mTransitionStyle = mTransitionStyle;
        bse.mName = mName;
        bse.mIndex = mIndex;
        bse.mAddToBackStack = true;
        bse.mBreadCrumbTitleRes = mBreadCrumbTitleRes;
        bse.mBreadCrumbTitleText = mBreadCrumbTitleText;
        bse.mBreadCrumbShortTitleRes = mBreadCrumbShortTitleRes;
        bse.mBreadCrumbShortTitleText = mBreadCrumbShortTitleText;
        bse.bumpBackStackNesting(1);
        return bse;
    }

    /** (non-Javadoc)
     * @see android.os.Parcelable#describeContents()
     */
    public int describeContents() {
        return 0;
    }

    /** (non-Javadoc)
     * @see android.os.Parcelable#writeToParcel(android.os.Parcel, int)
     */
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeIntArray(mOps);
        dest.writeInt(mTransition);
        dest.writeInt(mTransitionStyle);
        dest.writeString(mName);
        dest.writeInt(mIndex);
        dest.writeInt(mBreadCrumbTitleRes);
        TextUtils.writeToParcel(mBreadCrumbTitleText, dest, 0);
        dest.writeInt(mBreadCrumbShortTitleRes);
        TextUtils.writeToParcel(mBreadCrumbShortTitleText, dest, 0);
    }

    /** The Constant CREATOR. */
    public static final Parcelable.Creator<BackStackState> CREATOR
            = new Parcelable.Creator<BackStackState>() {
        public BackStackState createFromParcel(Parcel in) {
            return new BackStackState(in);
        }

        public BackStackState[] newArray(int size) {
            return new BackStackState[size];
        }
    };
}

/**
 * The Class BackStackRecord.
 *
 * @hide Entry of an operation on the fragment back stack.
 */
final class BackStackRecord extends FragmentTransaction implements
        FragmentManager.BackStackEntry, Runnable {
    
    /** The Constant TAG. */
    static final String TAG = "BackStackEntry";

    /** The m manager. */
    final FragmentManagerImpl mManager;

    /** The Constant OP_NULL. */
    static final int OP_NULL = 0;
    
    /** The Constant OP_ADD. */
    static final int OP_ADD = 1;
    
    /** The Constant OP_REPLACE. */
    static final int OP_REPLACE = 2;
    
    /** The Constant OP_REMOVE. */
    static final int OP_REMOVE = 3;
    
    /** The Constant OP_HIDE. */
    static final int OP_HIDE = 4;
    
    /** The Constant OP_SHOW. */
    static final int OP_SHOW = 5;
    
    /** The Constant OP_DETACH. */
    static final int OP_DETACH = 6;
    
    /** The Constant OP_ATTACH. */
    static final int OP_ATTACH = 7;

    /**
     * The Class Op.
     */
    static final class Op {
        
        /** The next. */
        Op next;
        
        /** The prev. */
        Op prev;
        
        /** The cmd. */
        int cmd;
        
        /** The fragment. */
        Fragment fragment;
        
        /** The enter anim. */
        int enterAnim;
        
        /** The exit anim. */
        int exitAnim;
        
        /** The pop enter anim. */
        int popEnterAnim;
        
        /** The pop exit anim. */
        int popExitAnim;
        
        /** The removed. */
        ArrayList<Fragment> removed;
    }

    /** The m head. */
    Op mHead;
    
    /** The m tail. */
    Op mTail;
    
    /** The m num op. */
    int mNumOp;
    
    /** The m enter anim. */
    int mEnterAnim;
    
    /** The m exit anim. */
    int mExitAnim;
    
    /** The m pop enter anim. */
    int mPopEnterAnim;
    
    /** The m pop exit anim. */
    int mPopExitAnim;
    
    /** The m transition. */
    int mTransition;
    
    /** The m transition style. */
    int mTransitionStyle;
    
    /** The m add to back stack. */
    boolean mAddToBackStack;
    
    /** The m allow add to back stack. */
    boolean mAllowAddToBackStack = true;
    
    /** The m name. */
    String mName;
    
    /** The m committed. */
    boolean mCommitted;
    
    /** The m index. */
    int mIndex;

    /** The m bread crumb title res. */
    int mBreadCrumbTitleRes;
    
    /** The m bread crumb title text. */
    CharSequence mBreadCrumbTitleText;
    
    /** The m bread crumb short title res. */
    int mBreadCrumbShortTitleRes;
    
    /** The m bread crumb short title text. */
    CharSequence mBreadCrumbShortTitleText;

    /**
     * Dump.
     *
     * @param prefix the prefix
     * @param fd the fd
     * @param writer the writer
     * @param args the args
     */
    public void dump(String prefix, FileDescriptor fd, PrintWriter writer, String[] args) {
        writer.print(prefix); writer.print("mName="); writer.print(mName);
                writer.print(" mIndex="); writer.print(mIndex);
                writer.print(" mCommitted="); writer.println(mCommitted);
        if (mTransition != FragmentTransaction.TRANSIT_NONE) {
            writer.print(prefix); writer.print("mTransition=#");
                    writer.print(Integer.toHexString(mTransition));
                    writer.print(" mTransitionStyle=#");
                    writer.println(Integer.toHexString(mTransitionStyle));
        }
        if (mEnterAnim != 0 || mExitAnim != 0) {
            writer.print(prefix); writer.print("mEnterAnim=#");
                    writer.print(Integer.toHexString(mEnterAnim));
                    writer.print(" mExitAnim=#");
                    writer.println(Integer.toHexString(mExitAnim));
        }
        if (mPopEnterAnim != 0 || mPopExitAnim != 0) {
            writer.print(prefix); writer.print("mPopEnterAnim=#");
                    writer.print(Integer.toHexString(mPopEnterAnim));
                    writer.print(" mPopExitAnim=#");
                    writer.println(Integer.toHexString(mPopExitAnim));
        }
        if (mBreadCrumbTitleRes != 0 || mBreadCrumbTitleText != null) {
            writer.print(prefix); writer.print("mBreadCrumbTitleRes=#");
                    writer.print(Integer.toHexString(mBreadCrumbTitleRes));
                    writer.print(" mBreadCrumbTitleText=");
                    writer.println(mBreadCrumbTitleText);
        }
        if (mBreadCrumbShortTitleRes != 0 || mBreadCrumbShortTitleText != null) {
            writer.print(prefix); writer.print("mBreadCrumbShortTitleRes=#");
                    writer.print(Integer.toHexString(mBreadCrumbShortTitleRes));
                    writer.print(" mBreadCrumbShortTitleText=");
                    writer.println(mBreadCrumbShortTitleText);
        }

        if (mHead != null) {
            writer.print(prefix); writer.println("Operations:");
            String innerPrefix = prefix + "    ";
            Op op = mHead;
            int num = 0;
            while (op != null) {
                writer.print(prefix); writer.print("  Op #"); writer.print(num);
                        writer.println(":");
                writer.print(innerPrefix); writer.print("cmd="); writer.print(op.cmd);
                        writer.print(" fragment="); writer.println(op.fragment);
                if (op.enterAnim != 0 || op.exitAnim != 0) {
                    writer.print(prefix); writer.print("enterAnim=#");
                            writer.print(Integer.toHexString(op.enterAnim));
                            writer.print(" exitAnim=#");
                            writer.println(Integer.toHexString(op.exitAnim));
                }
                if (op.popEnterAnim != 0 || op.popExitAnim != 0) {
                    writer.print(prefix); writer.print("popEnterAnim=#");
                            writer.print(Integer.toHexString(op.popEnterAnim));
                            writer.print(" popExitAnim=#");
                            writer.println(Integer.toHexString(op.popExitAnim));
                }
                if (op.removed != null && op.removed.size() > 0) {
                    for (int i = 0; i < op.removed.size(); i++) {
                        writer.print(innerPrefix);
                        if (op.removed.size() == 1) {
                            writer.print("Removed: ");
                        } else {
                            writer.println("Removed:");
                            writer.print(innerPrefix); writer.print("  #"); writer.print(num);
                                    writer.print(": ");
                        }
                        writer.println(op.removed.get(i));
                    }
                }
                op = op.next;
            }
        }
    }

    /**
     * Instantiates a new back stack record.
     *
     * @param manager the manager
     */
    public BackStackRecord(FragmentManagerImpl manager) {
        mManager = manager;
    }

    /**
     * (non-Javadoc).
     *
     * @return the id
     * @see android.support.v4.app.FragmentManager.BackStackEntry#getId()
     */
    public int getId() {
        return mIndex;
    }

    /**
     * (non-Javadoc).
     *
     * @return the bread crumb title res
     * @see android.support.v4.app.FragmentManager.BackStackEntry#getBreadCrumbTitleRes()
     */
    public int getBreadCrumbTitleRes() {
        return mBreadCrumbTitleRes;
    }

    /**
     * (non-Javadoc).
     *
     * @return the bread crumb short title res
     * @see android.support.v4.app.FragmentManager.BackStackEntry#getBreadCrumbShortTitleRes()
     */
    public int getBreadCrumbShortTitleRes() {
        return mBreadCrumbShortTitleRes;
    }

    /**
     * (non-Javadoc).
     *
     * @return the bread crumb title
     * @see android.support.v4.app.FragmentManager.BackStackEntry#getBreadCrumbTitle()
     */
    public CharSequence getBreadCrumbTitle() {
        if (mBreadCrumbTitleRes != 0) {
            return mManager.mActivity.getText(mBreadCrumbTitleRes);
        }
        return mBreadCrumbTitleText;
    }

    /**
     * (non-Javadoc).
     *
     * @return the bread crumb short title
     * @see android.support.v4.app.FragmentManager.BackStackEntry#getBreadCrumbShortTitle()
     */
    public CharSequence getBreadCrumbShortTitle() {
        if (mBreadCrumbShortTitleRes != 0) {
            return mManager.mActivity.getText(mBreadCrumbShortTitleRes);
        }
        return mBreadCrumbShortTitleText;
    }

    /**
     * Adds the op.
     *
     * @param op the op
     */
    void addOp(Op op) {
        if (mHead == null) {
            mHead = mTail = op;
        } else {
            op.prev = mTail;
            mTail.next = op;
            mTail = op;
        }
        op.enterAnim = mEnterAnim;
        op.exitAnim = mExitAnim;
        op.popEnterAnim = mPopEnterAnim;
        op.popExitAnim = mPopExitAnim;
        mNumOp++;
    }

    /**
     * (non-Javadoc).
     *
     * @param fragment the fragment
     * @param tag the tag
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#add(android.support.v4.app.Fragment, java.lang.String)
     */
    public FragmentTransaction add(Fragment fragment, String tag) {
        doAddOp(0, fragment, tag, OP_ADD);
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param containerViewId the container view id
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#add(int, android.support.v4.app.Fragment)
     */
    public FragmentTransaction add(int containerViewId, Fragment fragment) {
        doAddOp(containerViewId, fragment, null, OP_ADD);
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param containerViewId the container view id
     * @param fragment the fragment
     * @param tag the tag
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#add(int, android.support.v4.app.Fragment, java.lang.String)
     */
    public FragmentTransaction add(int containerViewId, Fragment fragment, String tag) {
        doAddOp(containerViewId, fragment, tag, OP_ADD);
        return this;
    }

    /**
     * Do add op.
     *
     * @param containerViewId the container view id
     * @param fragment the fragment
     * @param tag the tag
     * @param opcmd the opcmd
     */
    private void doAddOp(int containerViewId, Fragment fragment, String tag, int opcmd) {
        fragment.mFragmentManager = mManager;

        if (tag != null) {
            if (fragment.mTag != null && !tag.equals(fragment.mTag)) {
                throw new IllegalStateException("Can't change tag of fragment "
                        + fragment + ": was " + fragment.mTag
                        + " now " + tag);
            }
            fragment.mTag = tag;
        }

        if (containerViewId != 0) {
            if (fragment.mFragmentId != 0 && fragment.mFragmentId != containerViewId) {
                throw new IllegalStateException("Can't change container ID of fragment "
                        + fragment + ": was " + fragment.mFragmentId
                        + " now " + containerViewId);
            }
            fragment.mContainerId = fragment.mFragmentId = containerViewId;
        }

        Op op = new Op();
        op.cmd = opcmd;
        op.fragment = fragment;
        addOp(op);
    }

    /**
     * (non-Javadoc).
     *
     * @param containerViewId the container view id
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#replace(int, android.support.v4.app.Fragment)
     */
    public FragmentTransaction replace(int containerViewId, Fragment fragment) {
        return replace(containerViewId, fragment, null);
    }

    /**
     * (non-Javadoc).
     *
     * @param containerViewId the container view id
     * @param fragment the fragment
     * @param tag the tag
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#replace(int, android.support.v4.app.Fragment, java.lang.String)
     */
    public FragmentTransaction replace(int containerViewId, Fragment fragment, String tag) {
        if (containerViewId == 0) {
            throw new IllegalArgumentException("Must use non-zero containerViewId");
        }

        doAddOp(containerViewId, fragment, tag, OP_REPLACE);
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#remove(android.support.v4.app.Fragment)
     */
    public FragmentTransaction remove(Fragment fragment) {
        Op op = new Op();
        op.cmd = OP_REMOVE;
        op.fragment = fragment;
        addOp(op);

        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#hide(android.support.v4.app.Fragment)
     */
    public FragmentTransaction hide(Fragment fragment) {
        Op op = new Op();
        op.cmd = OP_HIDE;
        op.fragment = fragment;
        addOp(op);

        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#show(android.support.v4.app.Fragment)
     */
    public FragmentTransaction show(Fragment fragment) {
        Op op = new Op();
        op.cmd = OP_SHOW;
        op.fragment = fragment;
        addOp(op);

        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#detach(android.support.v4.app.Fragment)
     */
    public FragmentTransaction detach(Fragment fragment) {
        Op op = new Op();
        op.cmd = OP_DETACH;
        op.fragment = fragment;
        addOp(op);

        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param fragment the fragment
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#attach(android.support.v4.app.Fragment)
     */
    public FragmentTransaction attach(Fragment fragment) {
        Op op = new Op();
        op.cmd = OP_ATTACH;
        op.fragment = fragment;
        addOp(op);

        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param enter the enter
     * @param exit the exit
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setCustomAnimations(int, int)
     */
    public FragmentTransaction setCustomAnimations(int enter, int exit) {
        return setCustomAnimations(enter, exit, 0, 0);
    }

    /**
     * (non-Javadoc).
     *
     * @param enter the enter
     * @param exit the exit
     * @param popEnter the pop enter
     * @param popExit the pop exit
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setCustomAnimations(int, int, int, int)
     */
    public FragmentTransaction setCustomAnimations(int enter, int exit,
            int popEnter, int popExit) {
        mEnterAnim = enter;
        mExitAnim = exit;
        mPopEnterAnim = popEnter;
        mPopExitAnim = popExit;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param transition the transition
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setTransition(int)
     */
    public FragmentTransaction setTransition(int transition) {
        mTransition = transition;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param styleRes the style res
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setTransitionStyle(int)
     */
    public FragmentTransaction setTransitionStyle(int styleRes) {
        mTransitionStyle = styleRes;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param name the name
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#addToBackStack(java.lang.String)
     */
    public FragmentTransaction addToBackStack(String name) {
        if (!mAllowAddToBackStack) {
            throw new IllegalStateException(
                    "This FragmentTransaction is not allowed to be added to the back stack.");
        }
        mAddToBackStack = true;
        mName = name;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @return true, if is adds the to back stack allowed
     * @see android.support.v4.app.FragmentTransaction#isAddToBackStackAllowed()
     */
    public boolean isAddToBackStackAllowed() {
        return mAllowAddToBackStack;
    }

    /**
     * (non-Javadoc).
     *
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#disallowAddToBackStack()
     */
    public FragmentTransaction disallowAddToBackStack() {
        if (mAddToBackStack) {
            throw new IllegalStateException(
                    "This transaction is already being added to the back stack");
        }
        mAllowAddToBackStack = false;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param res the res
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setBreadCrumbTitle(int)
     */
    public FragmentTransaction setBreadCrumbTitle(int res) {
        mBreadCrumbTitleRes = res;
        mBreadCrumbTitleText = null;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param text the text
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setBreadCrumbTitle(java.lang.CharSequence)
     */
    public FragmentTransaction setBreadCrumbTitle(CharSequence text) {
        mBreadCrumbTitleRes = 0;
        mBreadCrumbTitleText = text;
        return this;
    }

    /**
     * (non-Javadoc).
     *
     * @param res the res
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setBreadCrumbShortTitle(int)
     */
    public FragmentTransaction setBreadCrumbShortTitle(int res) {
        mBreadCrumbShortTitleRes = res;
        mBreadCrumbShortTitleText = null;
        return this;
    }

    
    /**
     * (non-Javadoc).
     *
     * @param text the text
     * @return the fragment transaction
     * @see android.support.v4.app.FragmentTransaction#setBreadCrumbShortTitle(java.lang.CharSequence)
     */
    public FragmentTransaction setBreadCrumbShortTitle(CharSequence text) {
        mBreadCrumbShortTitleRes = 0;
        mBreadCrumbShortTitleText = text;
        return this;
    }

    /**
     * Bump back stack nesting.
     *
     * @param amt the amt
     */
    void bumpBackStackNesting(int amt) {
        if (!mAddToBackStack) {
            return;
        }
        if (FragmentManagerImpl.DEBUG) {
			Log.v(TAG, "Bump nesting in " + this
			        + " by " + amt);
		}
        Op op = mHead;
        while (op != null) {
            op.fragment.mBackStackNesting += amt;
            if (FragmentManagerImpl.DEBUG) {
				Log.v(TAG, "Bump nesting of "
				        + op.fragment + " to " + op.fragment.mBackStackNesting);
			}
            if (op.removed != null) {
                for (int i = op.removed.size() - 1; i >= 0; i--) {
                    Fragment r = op.removed.get(i);
                    r.mBackStackNesting += amt;
                    if (FragmentManagerImpl.DEBUG) {
						Log.v(TAG, "Bump nesting of "
						        + r + " to " + r.mBackStackNesting);
					}
                }
            }
            op = op.next;
        }
    }

    /**
     * (non-Javadoc).
     *
     * @return the int
     * @see android.support.v4.app.FragmentTransaction#commit()
     */
    public int commit() {
        return commitInternal(false);
    }

    /**
     * (non-Javadoc).
     *
     * @return the int
     * @see android.support.v4.app.FragmentTransaction#commitAllowingStateLoss()
     */
    public int commitAllowingStateLoss() {
        return commitInternal(true);
    }

    /**
     * Commit internal.
     *
     * @param allowStateLoss the allow state loss
     * @return the int
     */
    int commitInternal(boolean allowStateLoss) {
        if (mCommitted) {
			throw new IllegalStateException("commit already called");
		}
        if (FragmentManagerImpl.DEBUG) {
			Log.v(TAG, "Commit: " + this);
		}
        mCommitted = true;
        if (mAddToBackStack) {
            mIndex = mManager.allocBackStackIndex(this);
        } else {
            mIndex = -1;
        }
        mManager.enqueueAction(this, allowStateLoss);
        return mIndex;
    }

    /**
     * (non-Javadoc).
     *
     * @see java.lang.Runnable#run()
     */
    public void run() {
        if (FragmentManagerImpl.DEBUG) {
			Log.v(TAG, "Run: " + this);
		}

        if (mAddToBackStack) {
            if (mIndex < 0) {
                throw new IllegalStateException("addToBackStack() called after commit()");
            }
        }

        bumpBackStackNesting(1);

        Op op = mHead;
        while (op != null) {
            switch (op.cmd) {
                case OP_ADD: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.enterAnim;
                    mManager.addFragment(f, false);
                } break;
                case OP_REPLACE: {
                    Fragment f = op.fragment;
                    if (mManager.mAdded != null) {
                        for (int i = 0; i < mManager.mAdded.size(); i++) {
                            Fragment old = mManager.mAdded.get(i);
                            if (FragmentManagerImpl.DEBUG) {
								Log.v(TAG,
								        "OP_REPLACE: adding=" + f + " old=" + old);
							}
                            if (old.mContainerId == f.mContainerId) {
                                if (op.removed == null) {
                                    op.removed = new ArrayList<Fragment>();
                                }
                                op.removed.add(old);
                                old.mNextAnim = op.exitAnim;
                                if (mAddToBackStack) {
                                    old.mBackStackNesting += 1;
                                    if (FragmentManagerImpl.DEBUG) {
										Log.v(TAG, "Bump nesting of "
										        + old + " to " + old.mBackStackNesting);
									}
                                }
                                mManager.removeFragment(old, mTransition, mTransitionStyle);
                            }
                        }
                    }
                    f.mNextAnim = op.enterAnim;
                    mManager.addFragment(f, false);
                } break;
                case OP_REMOVE: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.exitAnim;
                    mManager.removeFragment(f, mTransition, mTransitionStyle);
                } break;
                case OP_HIDE: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.exitAnim;
                    mManager.hideFragment(f, mTransition, mTransitionStyle);
                } break;
                case OP_SHOW: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.enterAnim;
                    mManager.showFragment(f, mTransition, mTransitionStyle);
                } break;
                case OP_DETACH: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.exitAnim;
                    mManager.detachFragment(f, mTransition, mTransitionStyle);
                } break;
                case OP_ATTACH: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.enterAnim;
                    mManager.attachFragment(f, mTransition, mTransitionStyle);
                } break;
                default: {
                    throw new IllegalArgumentException("Unknown cmd: " + op.cmd);
                }
            }

            op = op.next;
        }

        mManager.moveToState(mManager.mCurState, mTransition,
                mTransitionStyle, true);

        if (mAddToBackStack) {
            mManager.addBackStackState(this);
        }
    }

    /**
     * Pop from back stack.
     *
     * @param doStateMove the do state move
     */
    public void popFromBackStack(boolean doStateMove) {
        if (FragmentManagerImpl.DEBUG) {
			Log.v(TAG, "popFromBackStack: " + this);
		}

        bumpBackStackNesting(-1);

        Op op = mTail;
        while (op != null) {
            switch (op.cmd) {
                case OP_ADD: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popExitAnim;
                    mManager.removeFragment(f,
                            FragmentManagerImpl.reverseTransit(mTransition),
                            mTransitionStyle);
                } break;
                case OP_REPLACE: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popExitAnim;
                    mManager.removeFragment(f,
                            FragmentManagerImpl.reverseTransit(mTransition),
                            mTransitionStyle);
                    if (op.removed != null) {
                        for (int i = 0; i < op.removed.size(); i++) {
                            Fragment old = op.removed.get(i);
                            old.mNextAnim = op.popEnterAnim;
                            mManager.addFragment(old, false);
                        }
                    }
                } break;
                case OP_REMOVE: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popEnterAnim;
                    mManager.addFragment(f, false);
                } break;
                case OP_HIDE: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popEnterAnim;
                    mManager.showFragment(f,
                            FragmentManagerImpl.reverseTransit(mTransition), mTransitionStyle);
                } break;
                case OP_SHOW: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popExitAnim;
                    mManager.hideFragment(f,
                            FragmentManagerImpl.reverseTransit(mTransition), mTransitionStyle);
                } break;
                case OP_DETACH: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popEnterAnim;
                    mManager.attachFragment(f,
                            FragmentManagerImpl.reverseTransit(mTransition), mTransitionStyle);
                } break;
                case OP_ATTACH: {
                    Fragment f = op.fragment;
                    f.mNextAnim = op.popEnterAnim;
                    mManager.detachFragment(f,
                            FragmentManagerImpl.reverseTransit(mTransition), mTransitionStyle);
                } break;
                default: {
                    throw new IllegalArgumentException("Unknown cmd: " + op.cmd);
                }
            }

            op = op.prev;
        }

        if (doStateMove) {
            mManager.moveToState(mManager.mCurState,
                    FragmentManagerImpl.reverseTransit(mTransition), mTransitionStyle, true);
        }

        if (mIndex >= 0) {
            mManager.freeBackStackIndex(mIndex);
            mIndex = -1;
        }
    }

    /**
     * (non-Javadoc).
     *
     * @return the name
     * @see android.support.v4.app.FragmentManager.BackStackEntry#getName()
     */
    public String getName() {
        return mName;
    }

    /**
     * Gets the transition.
     *
     * @return the transition
     */
    public int getTransition() {
        return mTransition;
    }

    /**
     * Gets the transition style.
     *
     * @return the transition style
     */
    public int getTransitionStyle() {
        return mTransitionStyle;
    }

    /**
     * (non-Javadoc).
     *
     * @return true, if is empty
     * @see android.support.v4.app.FragmentTransaction#isEmpty()
     */
    public boolean isEmpty() {
        return mNumOp == 0;
    }
}
