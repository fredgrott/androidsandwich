/*
 * Copyright 2011 Jake Wharton
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actionbarsherlock.internal.app;

import java.util.HashMap;

import com.actionbarsherlock.internal.view.menu.MenuInflaterImpl;
import com.actionbarsherlock.internal.view.menu.MenuItemWrapper;
import com.actionbarsherlock.internal.view.menu.MenuWrapper;
import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.ActionBar;
import android.support.v4.view.ActionMode;
import android.support.v4.view.Menu;
import android.view.View;
import android.widget.SpinnerAdapter;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionBarWrapper.
 */
public final class ActionBarWrapper {
    //No instances
    /**
     * Instantiates a new action bar wrapper.
     */
    private ActionBarWrapper() { }

    /**
     * Abstraction to get an instance of our implementing class.
     *
     * @param activity Parent activity.
     * @return {@code ActionBar} instance.
     */
    public static ActionBar createFor(Activity activity) {
        if (!(activity instanceof SherlockActivity)) {
            throw new RuntimeException("Activity must implement the SherlockActivity interface");
        }

        return new ActionBarWrapper.Impl(activity);
    }

    /**
     * Handler for Android's native {@link android.app.ActionBar}.
     */
    public static final class Impl extends ActionBar implements android.app.ActionBar.TabListener {
        /** Mapping between support listeners and native listeners. */
        private final HashMap<OnMenuVisibilityListener, android.app.ActionBar.OnMenuVisibilityListener> mMenuListenerMap = new HashMap<OnMenuVisibilityListener, android.app.ActionBar.OnMenuVisibilityListener>();

        /** The m activity. */
        private final Activity mActivity;

        /**
         * Instantiates a new impl.
         *
         * @param activity the activity
         */
        private Impl(Activity activity) {
            mActivity = activity;
        }


        /**
         * Get the native {@link ActionBar} instance.
         *
         * @return The action bar.
         */
        private android.app.ActionBar getActionBar() {
            return mActivity.getActionBar();
        }

        /**
         * Converts our Tab wrapper to a native version containing the wrapper
         * instance as its tag.
         *
         * @param tab Tab wrapper instance.
         * @return Native tab.
         */
        private android.app.ActionBar.Tab convertTabToNative(ActionBar.Tab tab) {
            return getActionBar().newTab()
                    .setCustomView(tab.getCustomView())
                    .setIcon(tab.getIcon())
                    .setTabListener(this)
                    .setTag(tab)
                    .setText(tab.getText());
        }

        /**
         * (non-Javadoc).
         *
         * @param tab the tab
         * @param ft the ft
         * @see android.app.ActionBar.TabListener#onTabReselected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabReselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            ActionBar.TabListener listener = ((ActionBar.Tab) tab.getTag()).getTabListener();
            if (listener != null) {
                listener.onTabReselected((ActionBar.Tab) tab.getTag(), null);
            }
        }

        /**
         * (non-Javadoc).
         *
         * @param tab the tab
         * @param ft the ft
         * @see android.app.ActionBar.TabListener#onTabSelected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabSelected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            ActionBar.TabListener listener = ((ActionBar.Tab) tab.getTag()).getTabListener();
            if (listener != null) {
                listener.onTabSelected((ActionBar.Tab) tab.getTag(), null);
            }
        }

        /**
         * (non-Javadoc).
         *
         * @param tab the tab
         * @param ft the ft
         * @see android.app.ActionBar.TabListener#onTabUnselected(android.app.ActionBar.Tab, android.app.FragmentTransaction)
         */
        @Override
        public void onTabUnselected(android.app.ActionBar.Tab tab, android.app.FragmentTransaction ft) {
            ActionBar.TabListener listener = ((ActionBar.Tab) tab.getTag()).getTabListener();
            if (listener != null) {
                listener.onTabUnselected((ActionBar.Tab) tab.getTag(), null);
            }
        }

        // ---------------------------------------------------------------------
        // ACTION MODE SUPPORT
        // ---------------------------------------------------------------------

        /**
         * (non-Javadoc).
         *
         * @param callback the callback
         * @return the action mode
         * @see android.support.v4.app.ActionBar#startActionMode(android.support.v4.view.ActionMode.Callback)
         */
        @Override
        protected ActionMode startActionMode(final ActionMode.Callback callback) {
            //We have to re-wrap the instances in every callback since the
            //wrapped instance is needed before we could have a change to
            //properly store it.
            return new ActionModeWrapper(mActivity,
                mActivity.startActionMode(new android.view.ActionMode.Callback() {
                    @Override
                    public boolean onPrepareActionMode(android.view.ActionMode mode, android.view.Menu menu) {
                        return callback.onPrepareActionMode(new ActionModeWrapper(mActivity, mode), new MenuWrapper(menu));
                    }

                    @Override
                    public void onDestroyActionMode(android.view.ActionMode mode) {
                        final ActionMode actionMode = new ActionModeWrapper(mActivity, mode);
                        callback.onDestroyActionMode(actionMode);

                        //Send the activity callback once the action mode callback has run.
                        //This type-check has already occurred in the action bar constructor.
                        ((SherlockActivity)mActivity).onActionModeFinished(actionMode);
                    }

                    /**
                     * (non-Javadoc).
                     *
                     * @param mode the mode
                     * @param menu the menu
                     * @return true, if successful
                     * @see android.view.ActionMode.Callback#onCreateActionMode(android.view.ActionMode, android.view.Menu)
                     */
                    @Override
                    public boolean onCreateActionMode(android.view.ActionMode mode, android.view.Menu menu) {
                        return callback.onCreateActionMode(new ActionModeWrapper(mActivity, mode), new MenuWrapper(menu));
                    }

                    /**
                     * (non-Javadoc).
                     *
                     * @param mode the mode
                     * @param item the item
                     * @return true, if successful
                     * @see android.view.ActionMode.Callback#onActionItemClicked(android.view.ActionMode, android.view.MenuItem)
                     */
                    @Override
                    public boolean onActionItemClicked(android.view.ActionMode mode, android.view.MenuItem item) {
                        return callback.onActionItemClicked(new ActionModeWrapper(mActivity, mode), new MenuItemWrapper(item));
                    }
                })
            );
        }

        /**
         * The Class ActionModeWrapper.
         */
        private static class ActionModeWrapper extends ActionMode {
            
            /** The m context. */
            private final Context mContext;
            
            /** The m action mode. */
            private final android.view.ActionMode mActionMode;

            /**
             * Instantiates a new action mode wrapper.
             *
             * @param context the context
             * @param actionMode the action mode
             */
            ActionModeWrapper(Context context, android.view.ActionMode actionMode) {
                mContext = context;
                mActionMode = actionMode;
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#finish()
             */
            @Override
            public void finish() {
                mActionMode.finish();
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#getCustomView()
             */
            @Override
            public View getCustomView() {
                return mActionMode.getCustomView();
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#getMenu()
             */
            @Override
            public Menu getMenu() {
                return new MenuWrapper(mActionMode.getMenu());
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#getMenuInflater()
             */
            @Override
            public MenuInflaterImpl getMenuInflater() {
                return new MenuInflaterImpl(mContext, null);
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#getSubtitle()
             */
            @Override
            public CharSequence getSubtitle() {
                return mActionMode.getSubtitle();
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#getTitle()
             */
            @Override
            public CharSequence getTitle() {
                return mActionMode.getTitle();
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#invalidate()
             */
            @Override
            public void invalidate() {
                mActionMode.invalidate();
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#setCustomView(android.view.View)
             */
            @Override
            public void setCustomView(View view) {
                mActionMode.setCustomView(view);
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#setSubtitle(int)
             */
            @Override
            public void setSubtitle(int resId) {
                mActionMode.setSubtitle(resId);
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#setSubtitle(java.lang.CharSequence)
             */
            @Override
            public void setSubtitle(CharSequence subtitle) {
                mActionMode.setSubtitle(subtitle);
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#setTitle(int)
             */
            @Override
            public void setTitle(int resId) {
                mActionMode.setTitle(resId);
            }

            /* (non-Javadoc)
             * @see android.support.v4.view.ActionMode#setTitle(java.lang.CharSequence)
             */
            @Override
            public void setTitle(CharSequence title) {
                mActionMode.setTitle(title);
            }
        }

        // ---------------------------------------------------------------------
        // ACTION BAR SUPPORT
        // ---------------------------------------------------------------------

        /**
         * The Class TabImpl.
         */
        private static class TabImpl extends ActionBar.Tab {
            
            /** The m action bar. */
            final ActionBarWrapper.Impl mActionBar;

            /** The m custom view. */
            View mCustomView;
            
            /** The m icon. */
            Drawable mIcon;
            
            /** The m listener. */
            ActionBar.TabListener mListener;
            
            /** The m tag. */
            Object mTag;
            
            /** The m text. */
            CharSequence mText;

            /**
             * Instantiates a new tab impl.
             *
             * @param actionBar the action bar
             */
            TabImpl(ActionBarWrapper.Impl actionBar) {
                mActionBar = actionBar;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#getCustomView()
             */
            @Override
            public View getCustomView() {
                return mCustomView;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#getIcon()
             */
            @Override
            public Drawable getIcon() {
                return mIcon;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#getPosition()
             */
            @Override
            public int getPosition() {
                final int tabCount = mActionBar.getTabCount();
                for (int i = 0; i < tabCount; i++) {
                    if (mActionBar.getTabAt(i).equals(this)) {
                        return i;
                    }
                }
                return ActionBar.Tab.INVALID_POSITION;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#getTabListener()
             */
            @Override
            public ActionBar.TabListener getTabListener() {
                return mListener;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#getTag()
             */
            @Override
            public Object getTag() {
                return mTag;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#getText()
             */
            @Override
            public CharSequence getText() {
                return mText;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#select()
             */
            @Override
            public void select() {
                mActionBar.selectTab(this);
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setCustomView(int)
             */
            @Override
            public ActionBar.Tab setCustomView(int layoutResId) {
                mCustomView = mActionBar.mActivity.getLayoutInflater().inflate(layoutResId, null);
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setCustomView(android.view.View)
             */
            @Override
            public ActionBar.Tab setCustomView(View view) {
                mCustomView = view;
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setIcon(android.graphics.drawable.Drawable)
             */
            @Override
            public ActionBar.Tab setIcon(Drawable icon) {
                mIcon = icon;
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setIcon(int)
             */
            @Override
            public ActionBar.Tab setIcon(int resId) {
                mIcon = mActionBar.mActivity.getResources().getDrawable(resId);
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setTabListener(android.support.v4.app.ActionBar.TabListener)
             */
            @Override
            public ActionBar.Tab setTabListener(TabListener listener) {
                mListener = listener;
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setTag(java.lang.Object)
             */
            @Override
            public ActionBar.Tab setTag(Object obj) {
                mTag = obj;
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setText(int)
             */
            @Override
            public ActionBar.Tab setText(int resId) {
                mText = mActionBar.mActivity.getResources().getString(resId);
                return this;
            }

            /* (non-Javadoc)
             * @see android.support.v4.app.ActionBar.Tab#setText(java.lang.CharSequence)
             */
            @Override
            public ActionBar.Tab setText(CharSequence text) {
                mText = text;
                return this;
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#addOnMenuVisibilityListener(android.support.v4.app.ActionBar.OnMenuVisibilityListener)
         */
        @Override
        public void addOnMenuVisibilityListener(final OnMenuVisibilityListener listener) {
            if ((listener != null) && !mMenuListenerMap.containsKey(listener)) {
                android.app.ActionBar.OnMenuVisibilityListener nativeListener = new android.app.ActionBar.OnMenuVisibilityListener() {
                    @Override
                    public void onMenuVisibilityChanged(boolean isVisible) {
                        listener.onMenuVisibilityChanged(isVisible);
                    }
                };
                mMenuListenerMap.put(listener, nativeListener);

                getActionBar().addOnMenuVisibilityListener(nativeListener);
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#addTab(android.support.v4.app.ActionBar.Tab)
         */
        @Override
        public void addTab(Tab tab) {
            getActionBar().addTab(convertTabToNative(tab));
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#addTab(android.support.v4.app.ActionBar.Tab, boolean)
         */
        @Override
        public void addTab(Tab tab, boolean setSelected) {
            getActionBar().addTab(convertTabToNative(tab), setSelected);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#addTab(android.support.v4.app.ActionBar.Tab, int)
         */
        @Override
        public void addTab(Tab tab, int position) {
            getActionBar().addTab(convertTabToNative(tab), position);
        }

        /**
         * (non-Javadoc).
         *
         * @param tab the tab
         * @param position the position
         * @param setSelected the set selected
         * @see android.support.v4.app.ActionBar#addTab(android.support.v4.app.ActionBar.Tab, int, boolean)
         */
        @Override
        public void addTab(ActionBar.Tab tab, int position, boolean setSelected) {
            getActionBar().addTab(convertTabToNative(tab), position, setSelected);
        }

        /**
         * (non-Javadoc).
         *
         * @return the custom view
         * @see android.support.v4.app.ActionBar#getCustomView()
         */
        @Override
        public View getCustomView() {
            return getActionBar().getCustomView();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getDisplayOptions()
         */
        @Override
        public int getDisplayOptions() {
            return getActionBar().getDisplayOptions();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getHeight()
         */
        @Override
        public int getHeight() {
            return getActionBar().getHeight();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getNavigationItemCount()
         */
        @Override
        public int getNavigationItemCount() {
            return getActionBar().getNavigationItemCount();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getNavigationMode()
         */
        @Override
        public int getNavigationMode() {
            return getActionBar().getNavigationMode();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getSelectedNavigationIndex()
         */
        @Override
        public int getSelectedNavigationIndex() {
            return getActionBar().getSelectedNavigationIndex();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getSelectedTab()
         */
        @Override
        public Tab getSelectedTab() {
            if (getActionBar().getSelectedTab() != null) {
                return (ActionBar.Tab)getActionBar().getSelectedTab().getTag();
            }
            return null;
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getSubtitle()
         */
        @Override
        public CharSequence getSubtitle() {
            return getActionBar().getSubtitle();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getTabAt(int)
         */
        @Override
        public ActionBar.Tab getTabAt(int index) {
            if (getActionBar().getTabAt(index) != null) {
                return (Tab)getActionBar().getTabAt(index).getTag();
            }
            return null;
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getTabCount()
         */
        @Override
        public int getTabCount() {
            return getActionBar().getTabCount();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#getTitle()
         */
        @Override
        public CharSequence getTitle() {
            return getActionBar().getTitle();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#hide()
         */
        @Override
        public void hide() {
            getActionBar().hide();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#isShowing()
         */
        @Override
        public boolean isShowing() {
            return getActionBar().isShowing();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#newTab()
         */
        @Override
        public ActionBar.Tab newTab() {
            return new TabImpl(this);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#removeAllTabs()
         */
        @Override
        public void removeAllTabs() {
            getActionBar().removeAllTabs();
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#removeOnMenuVisibilityListener(android.support.v4.app.ActionBar.OnMenuVisibilityListener)
         */
        @Override
        public void removeOnMenuVisibilityListener(OnMenuVisibilityListener listener) {
            if ((listener != null) && mMenuListenerMap.containsKey(listener)) {
                getActionBar().removeOnMenuVisibilityListener(
                    mMenuListenerMap.remove(listener)
                );
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#removeTab(android.support.v4.app.ActionBar.Tab)
         */
        @Override
        public void removeTab(Tab tab) {
            final int tabCount = getActionBar().getTabCount();
            for (int i = 0; i < tabCount; i++) {
                if (getActionBar().getTabAt(i).getTag().equals(tab)) {
                    getActionBar().removeTabAt(i);
                    break;
                }
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#removeTabAt(int)
         */
        @Override
        public void removeTabAt(int position) {
            getActionBar().removeTabAt(position);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#selectTab(android.support.v4.app.ActionBar.Tab)
         */
        @Override
        public void selectTab(ActionBar.Tab tab) {
            final int tabCount = getActionBar().getTabCount();
            for (int i = 0; i < tabCount; i++) {
                if (getActionBar().getTabAt(i).getTag().equals(tab)) {
                    getActionBar().setSelectedNavigationItem(i);
                    break;
                }
            }
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setBackgroundDrawable(android.graphics.drawable.Drawable)
         */
        @Override
        public void setBackgroundDrawable(Drawable d) {
            getActionBar().setBackgroundDrawable(d);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setCustomView(int)
         */
        @Override
        public void setCustomView(int resId) {
            getActionBar().setCustomView(resId);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setCustomView(android.view.View)
         */
        @Override
        public void setCustomView(View view) {
            getActionBar().setCustomView(view);
        }

        /**
         * (non-Javadoc).
         *
         * @param view the view
         * @param layoutParams the layout params
         * @see android.support.v4.app.ActionBar#setCustomView(android.view.View, android.support.v4.app.ActionBar.LayoutParams)
         */
        @Override
        public void setCustomView(View view, LayoutParams layoutParams) {
            android.app.ActionBar.LayoutParams nativeLayoutParams = new android.app.ActionBar.LayoutParams(layoutParams);
            nativeLayoutParams.gravity = layoutParams.gravity;
            getActionBar().setCustomView(view, nativeLayoutParams);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayHomeAsUpEnabled(boolean)
         */
        @Override
        public void setDisplayHomeAsUpEnabled(boolean showHomeAsUp) {
            getActionBar().setDisplayHomeAsUpEnabled(showHomeAsUp);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayOptions(int, int)
         */
        @Override
        public void setDisplayOptions(int options, int mask) {
            getActionBar().setDisplayOptions(options, mask);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayOptions(int)
         */
        @Override
        public void setDisplayOptions(int options) {
            getActionBar().setDisplayOptions(options);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayShowCustomEnabled(boolean)
         */
        @Override
        public void setDisplayShowCustomEnabled(boolean showCustom) {
            getActionBar().setDisplayShowCustomEnabled(showCustom);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayShowHomeEnabled(boolean)
         */
        @Override
        public void setDisplayShowHomeEnabled(boolean showHome) {
            getActionBar().setDisplayShowHomeEnabled(showHome);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayShowTitleEnabled(boolean)
         */
        @Override
        public void setDisplayShowTitleEnabled(boolean showTitle) {
            getActionBar().setDisplayShowTitleEnabled(showTitle);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setDisplayUseLogoEnabled(boolean)
         */
        @Override
        public void setDisplayUseLogoEnabled(boolean useLogo) {
            getActionBar().setDisplayUseLogoEnabled(useLogo);
        }

        /**
         * (non-Javadoc).
         *
         * @param adapter the adapter
         * @param callback the callback
         * @see android.support.v4.app.ActionBar#setListNavigationCallbacks(android.widget.SpinnerAdapter, android.support.v4.app.ActionBar.OnNavigationListener)
         */
        @Override
        public void setListNavigationCallbacks(SpinnerAdapter adapter, final OnNavigationListener callback) {
            getActionBar().setListNavigationCallbacks(adapter, new android.app.ActionBar.OnNavigationListener() {
                @Override
                public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                    if (callback != null) {
                        return callback.onNavigationItemSelected(itemPosition, itemId);
                    }
                    return false;
                }
            });
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setNavigationMode(int)
         */
        @Override
        public void setNavigationMode(int mode) {
            getActionBar().setNavigationMode(mode);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setSelectedNavigationItem(int)
         */
        @Override
        public void setSelectedNavigationItem(int position) {
            getActionBar().setSelectedNavigationItem(position);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setSubtitle(java.lang.CharSequence)
         */
        @Override
        public void setSubtitle(CharSequence subtitle) {
            getActionBar().setSubtitle(subtitle);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setSubtitle(int)
         */
        @Override
        public void setSubtitle(int resId) {
            getActionBar().setSubtitle(resId);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setTitle(java.lang.CharSequence)
         */
        @Override
        public void setTitle(CharSequence title) {
            getActionBar().setTitle(title);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#setTitle(int)
         */
        @Override
        public void setTitle(int resId) {
            getActionBar().setTitle(resId);
        }

        /* (non-Javadoc)
         * @see android.support.v4.app.ActionBar#show()
         */
        @Override
        public void show() {
            getActionBar().show();
        }
    }
}
