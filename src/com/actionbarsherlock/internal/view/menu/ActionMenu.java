/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.actionbarsherlock.internal.view.menu;

import java.util.ArrayList;
import java.util.List;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.v4.view.Menu;
import android.support.v4.view.MenuItem;
import android.support.v4.view.SubMenu;
import android.view.KeyEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionMenu.
 *
 * @hide
 */
public class ActionMenu implements Menu {
    
    /** The m context. */
    private Context mContext;

    /** The m is qwerty. */
    private boolean mIsQwerty;

    /** The m items. */
    private ArrayList<ActionMenuItem> mItems;

    /**
     * Instantiates a new action menu.
     *
     * @param context the context
     */
    public ActionMenu(Context context) {
        mContext = context;
        mItems = new ArrayList<ActionMenuItem>();
    }

    /**
     * Gets the context.
     *
     * @return the context
     */
    public Context getContext() {
        return mContext;
    }

    /**
     * (non-Javadoc).
     *
     * @param title the title
     * @return the menu item
     * @see android.support.v4.view.Menu#add(java.lang.CharSequence)
     */
    public MenuItem add(CharSequence title) {
        return add(0, 0, 0, title);
    }

    /**
     * (non-Javadoc).
     *
     * @param titleRes the title res
     * @return the menu item
     * @see android.support.v4.view.Menu#add(int)
     */
    public MenuItem add(int titleRes) {
        return add(0, 0, 0, titleRes);
    }

    /**
     * (non-Javadoc).
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param titleRes the title res
     * @return the menu item
     * @see android.support.v4.view.Menu#add(int, int, int, int)
     */
    public MenuItem add(int groupId, int itemId, int order, int titleRes) {
        return add(groupId, itemId, order, mContext.getResources().getString(titleRes));
    }

    /**
     * (non-Javadoc).
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param title the title
     * @return the menu item
     * @see android.support.v4.view.Menu#add(int, int, int, java.lang.CharSequence)
     */
    public MenuItem add(int groupId, int itemId, int order, CharSequence title) {
        ActionMenuItem item = new ActionMenuItem(getContext(),
                groupId, itemId, 0, order, title);
        mItems.add(order, item);
        return item;
    }

    /**
     * (non-Javadoc).
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param caller the caller
     * @param specifics the specifics
     * @param intent the intent
     * @param flags the flags
     * @param outSpecificItems the out specific items
     * @return the int
     * @see android.view.Menu#addIntentOptions(int, int, int, android.content.ComponentName, android.content.Intent[], android.content.Intent, int, android.view.MenuItem[])
     */
    public int addIntentOptions(int groupId, int itemId, int order,
            ComponentName caller, Intent[] specifics, Intent intent, int flags,
            android.view.MenuItem[] outSpecificItems) {
        PackageManager pm = mContext.getPackageManager();
        final List<ResolveInfo> lri =
                pm.queryIntentActivityOptions(caller, specifics, intent, 0);
        final int N = lri != null ? lri.size() : 0;

        if ((flags & FLAG_APPEND_TO_GROUP) == 0) {
            removeGroup(groupId);
        }

        for (int i = 0; i < N; i++) {
            final ResolveInfo ri = lri.get(i);
            Intent rintent = new Intent(
                ri.specificIndex < 0 ? intent : specifics[ri.specificIndex]);
            rintent.setComponent(new ComponentName(
                    ri.activityInfo.applicationInfo.packageName,
                    ri.activityInfo.name));
            final MenuItem item = add(groupId, itemId, order, ri.loadLabel(pm))
                    .setIcon(ri.loadIcon(pm))
                    .setIntent(rintent);
            if (outSpecificItems != null && ri.specificIndex >= 0) {
                outSpecificItems[ri.specificIndex] = item;
            }
        }

        return N;
    }

    /**
     * (non-Javadoc).
     *
     * @param title the title
     * @return the sub menu
     * @see android.support.v4.view.Menu#addSubMenu(java.lang.CharSequence)
     */
    public SubMenu addSubMenu(CharSequence title) {
        // TODO Implement submenus
        return null;
    }

    /**
     * (non-Javadoc).
     *
     * @param titleRes the title res
     * @return the sub menu
     * @see android.support.v4.view.Menu#addSubMenu(int)
     */
    public SubMenu addSubMenu(int titleRes) {
        // TODO Implement submenus
        return null;
    }

    /**
     * (non-Javadoc).
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param title the title
     * @return the sub menu
     * @see android.support.v4.view.Menu#addSubMenu(int, int, int, java.lang.CharSequence)
     */
    public SubMenu addSubMenu(int groupId, int itemId, int order,
            CharSequence title) {
        // TODO Implement submenus
        return null;
    }

    /**
     * (non-Javadoc).
     *
     * @param groupId the group id
     * @param itemId the item id
     * @param order the order
     * @param titleRes the title res
     * @return the sub menu
     * @see android.support.v4.view.Menu#addSubMenu(int, int, int, int)
     */
    public SubMenu addSubMenu(int groupId, int itemId, int order, int titleRes) {
        // TODO Implement submenus
        return null;
    }

    /**
     * (non-Javadoc).
     *
     * @see android.view.Menu#clear()
     */
    public void clear() {
        mItems.clear();
    }

    /**
     * (non-Javadoc).
     *
     * @see android.view.Menu#close()
     */
    public void close() {
    }

    /**
     * Find item index.
     *
     * @param id the id
     * @return the int
     */
    private int findItemIndex(int id) {
        final ArrayList<ActionMenuItem> items = mItems;
        final int itemCount = items.size();
        for (int i = 0; i < itemCount; i++) {
            if (items.get(i).getItemId() == id) {
                return i;
            }
        }

        return -1;
    }

    /**
     * (non-Javadoc).
     *
     * @param id the id
     * @return the menu item
     * @see android.support.v4.view.Menu#findItem(int)
     */
    public MenuItem findItem(int id) {
        return mItems.get(findItemIndex(id));
    }

    /**
     * (non-Javadoc).
     *
     * @param index the index
     * @return the item
     * @see android.support.v4.view.Menu#getItem(int)
     */
    public MenuItem getItem(int index) {
        return mItems.get(index);
    }

    /**
     * (non-Javadoc).
     *
     * @return true, if successful
     * @see android.view.Menu#hasVisibleItems()
     */
    public boolean hasVisibleItems() {
        final ArrayList<ActionMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int i = 0; i < itemCount; i++) {
            if (items.get(i).isVisible()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Find item with shortcut.
     *
     * @param keyCode the key code
     * @param event the event
     * @return the action menu item
     */
    private ActionMenuItem findItemWithShortcut(int keyCode,
    		KeyEvent event) {
        // TODO Make this smarter.
        final boolean qwerty = mIsQwerty;
        final ArrayList<ActionMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int i = 0; i < itemCount; i++) {
            ActionMenuItem item = items.get(i);
            final char shortcut = qwerty ? item.getAlphabeticShortcut() 
            		:
                    item.getNumericShortcut();
            if (keyCode == shortcut) {
                return item;
            }
        }
        return null;
    }

    /**
     * (non-Javadoc).
     *
     * @param keyCode the key code
     * @param event the event
     * @return true, if is shortcut key
     * @see android.view.Menu#isShortcutKey(int, android.view.KeyEvent)
     */
    public boolean isShortcutKey(int keyCode, KeyEvent event) {
        return findItemWithShortcut(keyCode, event) != null;
    }

    /**
     * (non-Javadoc).
     *
     * @param id the id
     * @param flags the flags
     * @return true, if successful
     * @see android.view.Menu#performIdentifierAction(int, int)
     */
    public boolean performIdentifierAction(int id, int flags) {
        final int index = findItemIndex(id);
        if (index < 0) {
            return false;
        }

        return mItems.get(index).invoke();
    }

    /* (non-Javadoc)
     * @see android.view.Menu#performShortcut(int, android.view.KeyEvent, int)
     */
    public boolean performShortcut(int keyCode, KeyEvent event, int flags) {
        ActionMenuItem item = findItemWithShortcut(keyCode, event);
        if (item == null) {
            return false;
        }

        return item.invoke();
    }

    /**
     * (non-Javadoc).
     *
     * @param groupId the group id
     * @see android.view.Menu#removeGroup(int)
     */
    public void removeGroup(int groupId) {
        final ArrayList<ActionMenuItem> items = mItems;
        int itemCount = items.size();
        int i = 0;
        while (i < itemCount) {
            if (items.get(i).getGroupId() == groupId) {
                items.remove(i);
                itemCount--;
            } else {
                i++;
            }
        }
    }

    /**
     * (non-Javadoc).
     *
     * @param id the id
     * @see android.view.Menu#removeItem(int)
     */
    public void removeItem(int id) {
        mItems.remove(findItemIndex(id));
    }

    /**
     * (non-Javadoc).
     *
     * @param group the group
     * @param checkable the checkable
     * @param exclusive the exclusive
     * @see android.view.Menu#setGroupCheckable(int, boolean, boolean)
     */
    public void setGroupCheckable(int group, boolean checkable,
            boolean exclusive) {
        final ArrayList<ActionMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int i = 0; i < itemCount; i++) {
            ActionMenuItem item = items.get(i);
            if (item.getGroupId() == group) {
                item.setCheckable(checkable);
                item.setExclusiveCheckable(exclusive);
            }
        }
    }

    /**
     * (non-Javadoc).
     *
     * @param group the group
     * @param enabled the enabled
     * @see android.view.Menu#setGroupEnabled(int, boolean)
     */
    public void setGroupEnabled(int group, boolean enabled) {
        final ArrayList<ActionMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int i = 0; i < itemCount; i++) {
            ActionMenuItem item = items.get(i);
            if (item.getGroupId() == group) {
                item.setEnabled(enabled);
            }
        }
    }

    /**
     * (non-Javadoc).
     *
     * @param group the group
     * @param visible the visible
     * @see android.view.Menu#setGroupVisible(int, boolean)
     */
    public void setGroupVisible(int group, boolean visible) {
        final ArrayList<ActionMenuItem> items = mItems;
        final int itemCount = items.size();

        for (int i = 0; i < itemCount; i++) {
            ActionMenuItem item = items.get(i);
            if (item.getGroupId() == group) {
                item.setVisible(visible);
            }
        }
    }

    /**
     * (non-Javadoc).
     *
     * @param isQwerty the new qwerty mode
     * @see android.view.Menu#setQwertyMode(boolean)
     */
    public void setQwertyMode(boolean isQwerty) {
        mIsQwerty = isQwerty;
    }

    /**
     * (non-Javadoc).
     *
     * @return the int
     * @see android.view.Menu#size()
     */
    public int size() {
        return mItems.size();
    }
}
