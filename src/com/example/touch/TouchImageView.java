/*
 * TouchImageView.java
 * By: Michael Ortiz
 * -------------------
 * Extends Android ImageView to include pinch zooming and panning.
 */

package com.example.touch;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.ScaleGestureDetector;
import android.view.View;
import android.widget.ImageView;

// TODO: Auto-generated Javadoc
/**
 * The Class TouchImageView.
 */
public class TouchImageView extends ImageView {

    /** The matrix. */
    Matrix matrix = new Matrix();

    // We can be in one of these 3 states
    /** The Constant NONE. */
    static final int NONE = 0;
    
    /** The Constant DRAG. */
    static final int DRAG = 1;
    
    /** The Constant ZOOM. */
    static final int ZOOM = 2;
    
    /** The mode. */
    int mode = NONE;

    // Remember some things for zooming
    /** The last. */
    PointF last = new PointF();
    
    /** The start. */
    PointF start = new PointF();
    
    /** The min scale. */
    float minScale = 1f;
    
    /** The max scale. */
    float maxScale = 3f;
    
    /** The m. */
    float[] m;
    
    /** The redundant y space. */
    float redundantXSpace, redundantYSpace;
    
    /** The height. */
    float width, height;
    
    /** The Constant CLICK. */
    static final int CLICK = 3;
    
    /** The save scale. */
    float saveScale = 1f;
    
    /** The bm height. */
    float right, bottom, origWidth, origHeight, bmWidth, bmHeight;
    
    /** The m scale detector. */
    ScaleGestureDetector mScaleDetector;
    
    /** The context. */
    Context context;


    /**
     * Instantiates a new touch image view.
     *
     * @param context the context
     */
    public TouchImageView(Context context) {
        super(context);
        super.setClickable(true);
        this.context = context;
        mScaleDetector = new ScaleGestureDetector(context, new ScaleListener());
        matrix.setTranslate(1f, 1f);
        m = new float[9];
        setImageMatrix(matrix);
        setScaleType(ScaleType.MATRIX);

        setOnTouchListener(new OnTouchListener() {

            /**
             * (non-Javadoc).
             *
             * @param v the v
             * @param event the event
             * @return true, if successful
             * @see android.view.View.OnTouchListener#onTouch(android.view.View, android.view.MotionEvent)
             */
            @Override
            public boolean onTouch(View v, MotionEvent event) {
            	mScaleDetector.onTouchEvent(event);

            	matrix.getValues(m);
            	float x = m[Matrix.MTRANS_X];
            	float y = m[Matrix.MTRANS_Y];
            	PointF curr = new PointF(event.getX(), event.getY());
            	
            	switch (event.getAction()) {
	            	case MotionEvent.ACTION_DOWN:
	                    last.set(event.getX(), event.getY());
	                    start.set(last);
	                    mode = DRAG;
	                    break;
	            	case MotionEvent.ACTION_MOVE:
	            		if (mode == DRAG) {
	            			float deltaX = curr.x - last.x;
	            			float deltaY = curr.y - last.y;
	            			float scaleWidth = Math.round(origWidth * saveScale);
	            			float scaleHeight = Math.round(origHeight * saveScale);
            				if (scaleWidth < width) {
	            				deltaX = 0;
	            				if (y + deltaY > 0) {
									deltaY = -y;
								} else if (y + deltaY < -bottom) {
									deltaY = -(y + bottom);
								} 
            				} else if (scaleHeight < height) {
	            				deltaY = 0;
	            				if (x + deltaX > 0) {
									deltaX = -x;
								} else if (x + deltaX < -right) {
									deltaX = -(x + right);
								}
            				} else {
	            				if (x + deltaX > 0) {
									deltaX = -x;
								} else if (x + deltaX < -right) {
									deltaX = -(x + right);
								}
		            			
	            				if (y + deltaY > 0) {
									deltaY = -y;
								} else if (y + deltaY < -bottom) {
									deltaY = -(y + bottom);
								}
	            			}
                        	matrix.postTranslate(deltaX, deltaY);
                        	last.set(curr.x, curr.y);
	                    }
	            		break;
	            		
	            	case MotionEvent.ACTION_UP:
	            		mode = NONE;
	            		int xDiff = (int) Math.abs(curr.x - start.x);
	                    int yDiff = (int) Math.abs(curr.y - start.y);
	                    if (xDiff < CLICK && yDiff < CLICK) {
							performClick();
						}
	            		break;
	            		
	            	case MotionEvent.ACTION_POINTER_UP:
	            		mode = NONE;
	            		break;
				default:
					break;
            	}
                setImageMatrix(matrix);
                invalidate();
                return true; // indicate event was handled
            }

        });
    }

    /* (non-Javadoc)
     * @see android.widget.ImageView#setImageBitmap(android.graphics.Bitmap)
     */
    @Override
    public void setImageBitmap(Bitmap bm) { 
        super.setImageBitmap(bm);
        bmWidth = bm.getWidth();
        bmHeight = bm.getHeight();
    }
    
    /**
     * Sets the max zoom.
     *
     * @param x the new max zoom
     */
    public void setMaxZoom(float x) {
    	maxScale = x;
    }
    
    /**
     * The listener interface for receiving scale events.
     * The class that is interested in processing a scale
     * event implements this interface, and the object created
     * with that class is registered with a component using the
     * component's <code>addScaleListener<code> method. When
     * the scale event occurs, that object's appropriate
     * method is invoked.
     *
     * @see ScaleEvent
     */
    private class ScaleListener extends ScaleGestureDetector.SimpleOnScaleGestureListener {
    	
	    /**
    	 * (non-Javadoc).
    	 *
    	 * @param detector the detector
    	 * @return true, if successful
    	 * @see android.view.ScaleGestureDetector.SimpleOnScaleGestureListener#onScaleBegin(android.view.ScaleGestureDetector)
    	 */
	    @Override
    	public boolean onScaleBegin(ScaleGestureDetector detector) {
    		mode = ZOOM;
    		return true;
    	}
    	
		/**
		 * (non-Javadoc).
		 *
		 * @param detector the detector
		 * @return true, if successful
		 * @see android.view.ScaleGestureDetector.SimpleOnScaleGestureListener#onScale(android.view.ScaleGestureDetector)
		 */
		@Override
	    public boolean onScale(ScaleGestureDetector detector) {
			float mScaleFactor = (float) Math.min(Math.max(.95f, detector.getScaleFactor()), 1.05);
		 	float origScale = saveScale;
	        saveScale *= mScaleFactor;
	        if (saveScale > maxScale) {
	        	saveScale = maxScale;
	        	mScaleFactor = maxScale / origScale;
	        } else if (saveScale < minScale) {
	        	saveScale = minScale;
	        	mScaleFactor = minScale / origScale;
	        }
        	right = width * saveScale - width - (2 * redundantXSpace * saveScale);
            bottom = height * saveScale - height - (2 * redundantYSpace * saveScale);
        	if (origWidth * saveScale <= width || origHeight * saveScale <= height) {
        		matrix.postScale(mScaleFactor, mScaleFactor, width / 2, height / 2);
            	if (mScaleFactor < 1) {
            		matrix.getValues(m);
            		float x = m[Matrix.MTRANS_X];
                	float y = m[Matrix.MTRANS_Y];
                	if (mScaleFactor < 1) {
        	        	if (Math.round(origWidth * saveScale) < width) {
        	        		if (y < -bottom) {
								matrix.postTranslate(0, -(y + bottom));
							} else if (y > 0) {
								matrix.postTranslate(0, -y);
							}
        	        	} else {
	                		if (x < -right) {
								matrix.postTranslate(-(x + right), 0);
							} else if (x > 0) {
								matrix.postTranslate(-x, 0);
							}
        	        	}
                	}
            	}
        	} else {
            	matrix.postScale(mScaleFactor, mScaleFactor, detector.getFocusX(), detector.getFocusY());
            	matrix.getValues(m);
            	float x = m[Matrix.MTRANS_X];
            	float y = m[Matrix.MTRANS_Y];
            	if (mScaleFactor < 1) {
    	        	if (x < -right) {
						matrix.postTranslate(-(x + right), 0);
					} else if (x > 0) {
						matrix.postTranslate(-x, 0);
					}
    	        	if (y < -bottom) {
						matrix.postTranslate(0, -(y + bottom));
					} else if (y > 0) {
						matrix.postTranslate(0, -y);
					}
            	}
        	}
	        return true;
	        
	    }
	}
    
    /**
     * (non-Javadoc).
     *
     * @param widthMeasureSpec the width measure spec
     * @param heightMeasureSpec the height measure spec
     * @see android.widget.ImageView#onMeasure(int, int)
     */
    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        width = MeasureSpec.getSize(widthMeasureSpec);
        height = MeasureSpec.getSize(heightMeasureSpec);
        //Fit to screen.
        float scale;
        float scaleX =  (float) width / (float) bmWidth;
        float scaleY = (float) height / (float) bmHeight;
        scale = Math.min(scaleX, scaleY);
        matrix.setScale(scale, scale);
        setImageMatrix(matrix);
        saveScale = 1f;

        // Center the image
        redundantYSpace = (float) height - (scale * (float) bmHeight);
        redundantXSpace = (float) width - (scale * (float) bmWidth);
        redundantYSpace /= (float) 2;
        redundantXSpace /= (float) 2;

        matrix.postTranslate(redundantXSpace, redundantYSpace);
        
        origWidth = width - 2 * redundantXSpace;
        origHeight = height - 2 * redundantYSpace;
        right = width * saveScale - width - (2 * redundantXSpace * saveScale);
        bottom = height * saveScale - height - (2 * redundantYSpace * saveScale);
        setImageMatrix(matrix);
    }
}