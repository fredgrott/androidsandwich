package com.nineoldandroids.view.animation;

import java.util.WeakHashMap;
import android.graphics.Camera;
import android.graphics.Matrix;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.Transformation;

// TODO: Auto-generated Javadoc
/**
 * A proxy class to allow for modifying post-3.0 view properties on all pre-3.0
 * platforms. <strong>DO NOT</strong> wrap your views with this class if you
 * are using {@code ObjectAnimator} as it will handle that itself.
 */
public final class AnimatorProxy extends Animation {
    /** Whether or not the current running platform needs to be proxied. */
    public static final boolean NEEDS_PROXY = Integer.valueOf(Build.VERSION.SDK).intValue() < Build.VERSION_CODES.HONEYCOMB;

    /** The Constant PROXIES. */
    private static final WeakHashMap<View, AnimatorProxy> PROXIES =
            new WeakHashMap<View, AnimatorProxy>();

    /**
     * Create a proxy to allow for modifying post-3.0 view properties on all
     * pre-3.0 platforms. <strong>DO NOT</strong> wrap your views if you are
     * using {@code ObjectAnimator} as it will handle that itself.
     *
     * @param view View to wrap.
     * @return Proxy to post-3.0 properties.
     */
    public static AnimatorProxy wrap(View view) {
        AnimatorProxy proxy = PROXIES.get(view);
        if (proxy == null) {
            proxy = new AnimatorProxy(view);
            PROXIES.put(view, proxy);
        }
        return proxy;
    }

    /** The m view. */
    private final View mView;
    
    /** The m view parent. */
    private final ViewGroup mViewParent;
    
    /** The m camera. */
    private final Camera mCamera;
    
    /** The m has pivot. */
    private boolean mHasPivot = false;

    /** The m alpha. */
    private float mAlpha = 1;
    
    /** The m pivot x. */
    private float mPivotX = 0;
    
    /** The m pivot y. */
    private float mPivotY = 0;
    
    /** The m rotation x. */
    private float mRotationX = 0;
    
    /** The m rotation y. */
    private float mRotationY = 0;
    
    /** The m rotation z. */
    private float mRotationZ = 0;
    
    /** The m scale x. */
    private float mScaleX = 1;
    
    /** The m scale y. */
    private float mScaleY = 1;
    
    /** The m translation x. */
    private float mTranslationX = 0;
    
    /** The m translation y. */
    private float mTranslationY = 0;

    /**
     * Instantiates a new animator proxy.
     *
     * @param view the view
     */
    private AnimatorProxy(View view) {
        setDuration(0); //perform transformation immediately
        setFillAfter(true); //persist transformation beyond duration
        view.setAnimation(this);
        mView = view;
        mViewParent = (ViewGroup)view.getParent();
        mCamera = new Camera();
    }

    /**
     * Gets the alpha.
     *
     * @return the alpha
     */
    public float getAlpha() {
        return mAlpha;
    }
    
    /**
     * Sets the alpha.
     *
     * @param alpha the new alpha
     */
    public void setAlpha(float alpha) {
        mAlpha = alpha;
        mView.invalidate();
    }
    
    /**
     * Gets the pivot x.
     *
     * @return the pivot x
     */
    public float getPivotX() {
        return mPivotX;
    }
    
    /**
     * Sets the pivot x.
     *
     * @param pivotX the new pivot x
     */
    public void setPivotX(float pivotX) {
        mHasPivot = true;
        if (mPivotX != pivotX) {
            mPivotX = pivotX;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the pivot y.
     *
     * @return the pivot y
     */
    public float getPivotY() {
        return mPivotY;
    }
    
    /**
     * Sets the pivot y.
     *
     * @param pivotY the new pivot y
     */
    public void setPivotY(float pivotY) {
        mHasPivot = true;
        if (mPivotY != pivotY) {
            mPivotY = pivotY;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the rotation.
     *
     * @return the rotation
     */
    public float getRotation() {
        return mRotationZ;
    }
    
    /**
     * Sets the rotation.
     *
     * @param rotation the new rotation
     */
    public void setRotation(float rotation) {
        if (mRotationZ != rotation) {
            mRotationZ = rotation;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the rotation x.
     *
     * @return the rotation x
     */
    public float getRotationX() {
        return mRotationX;
    }
    
    /**
     * Sets the rotation x.
     *
     * @param rotationX the new rotation x
     */
    public void setRotationX(float rotationX) {
        if (mRotationX != rotationX) {
            mRotationX = rotationX;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the rotation y.
     *
     * @return the rotation y
     */
    public float getRotationY() {
        return mRotationY;
    }
    
    /**
     * Sets the rotation y.
     *
     * @param rotationY the new rotation y
     */
    public void setRotationY(float rotationY) {
        if (mRotationY != rotationY) {
            mRotationY = rotationY;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the scale x.
     *
     * @return the scale x
     */
    public float getScaleX() {
        return mScaleX;
    }
    
    /**
     * Sets the scale x.
     *
     * @param scaleX the new scale x
     */
    public void setScaleX(float scaleX) {
        if (mScaleX != scaleX) {
            mScaleX = scaleX;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the scale y.
     *
     * @return the scale y
     */
    public float getScaleY() {
        return mScaleY;
    }
    
    /**
     * Sets the scale y.
     *
     * @param scaleY the new scale y
     */
    public void setScaleY(float scaleY) {
        if (mScaleY != scaleY) {
            mScaleY = scaleY;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the scroll x.
     *
     * @return the scroll x
     */
    public int getScrollX() {
        return mView.getScrollX();
    }
    
    /**
     * Sets the scroll x.
     *
     * @param value the new scroll x
     */
    public void setScrollX(int value) {
        mView.scrollTo(value, mView.getScrollY());
    }
    
    /**
     * Gets the scroll y.
     *
     * @return the scroll y
     */
    public int getScrollY() {
        return mView.getScrollY();
    }
    
    /**
     * Sets the scroll y.
     *
     * @param value the new scroll y
     */
    public void setScrollY(int value) {
        mView.scrollTo(mView.getScrollY(), value);
    }
    
    /**
     * Gets the translation x.
     *
     * @return the translation x
     */
    public float getTranslationX() {
        return mTranslationX;
    }
    
    /**
     * Sets the translation x.
     *
     * @param translationX the new translation x
     */
    public void setTranslationX(float translationX) {
        if (mTranslationX != translationX) {
            mTranslationX = translationX;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the translation y.
     *
     * @return the translation y
     */
    public float getTranslationY() {
        return mTranslationY;
    }
    
    /**
     * Sets the translation y.
     *
     * @param translationY the new translation y
     */
    public void setTranslationY(float translationY) {
        if (mTranslationY != translationY) {
            mTranslationY = translationY;
            mViewParent.invalidate();
        }
    }
    
    /**
     * Gets the x.
     *
     * @return the x
     */
    public float getX() {
        return mView.getLeft() + mTranslationX;
    }
    
    /**
     * Sets the x.
     *
     * @param x the new x
     */
    public void setX(float x) {
        setTranslationX(x - mView.getLeft());
    }
    
    /**
     * Gets the y.
     *
     * @return the y
     */
    public float getY() {
        return mView.getTop() + mTranslationY;
    }
    
    /**
     * Sets the y.
     *
     * @param y the new y
     */
    public void setY(float y) {
        setTranslationY(y - mView.getTop());
    }

    /* (non-Javadoc)
     * @see android.view.animation.Animation#applyTransformation(float, android.view.animation.Transformation)
     */
    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        t.setAlpha(mAlpha);

        final View view = mView;
        final float w = view.getWidth();
        final float h = view.getHeight();
        final Matrix m = t.getMatrix();

        final float rX = mRotationX;
        final float rY = mRotationY;
        final float rZ = mRotationZ;
        if ((rX != 0) || (rY != 0) || (rZ != 0)) {
            final Camera camera = mCamera;
            final boolean hasPivot = mHasPivot;
            final float pX = hasPivot ? mPivotX : w / 2f;
            final float pY = hasPivot ? mPivotY : h / 2f;
            camera.save();
            camera.rotateX(rX);
            camera.rotateY(rY);
            camera.rotateZ(-rZ);
            camera.getMatrix(m);
            camera.restore();
            m.preTranslate(-pX, -pY);
            m.postTranslate(pX, pY);
        }

        final float sX = mScaleX;
        final float sY = mScaleY;
        if ((sX != 0) || (sX != 0)) {
            final float deltaSX = ((sX * w) - w) / 2f;
            final float deltaSY = ((sY * h) - h) / 2f;
            m.postScale(sX, sY);
            m.postTranslate(-deltaSX, -deltaSY);
        }
        m.postTranslate(mTranslationX, mTranslationY);
    }
}
