package org.bitbucket.fredgrott.androidsandwich.cache;

import android.content.Context;
import android.graphics.Bitmap;


public class ConstantContextLruBitmapCache {

	ConstantContextLruCache<String, Bitmap> cache;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ConstantContextLruBitmapCache(int maxSize, Context context) {
		cache = new ConstantContextLruCache(maxSize, context);
	}
	
	@SuppressWarnings("unchecked")
	public Bitmap getBitmap(String string){
		return (Bitmap) cache.get(string);
	}
	
	@SuppressWarnings("unchecked")
	public void putBitmap(String string, Bitmap bitmap){
		cache.put(string,  bitmap);
	}
	
	public void clean(){
		cache.evictAll();
	}
}
