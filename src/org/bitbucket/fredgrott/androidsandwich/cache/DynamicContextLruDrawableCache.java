package org.bitbucket.fredgrott.androidsandwich.cache;

import android.graphics.drawable.Drawable;
import android.support.v4.util.LruCache;
/**
 * pre jwilson's suggestion
 * @author fredgrott
 *
 */
public class DynamicContextLruDrawableCache {
	LruCache<ContextAndKey<String>, Drawable> cache;
	
	public DynamicContextLruDrawableCache(int maxSize) {
		cache = new LruCache<ContextAndKey<String>, Drawable>(maxSize);
	}
	
	public Drawable getDrawable(ContextAndKey<String> string){
		return cache.get(string);
	}
	
	public void putDrawable(ContextAndKey<String> string, Drawable draw){
		cache.put(string, draw);
		
	}
	
	public void clean(){
		cache.evictAll();
	}
}
