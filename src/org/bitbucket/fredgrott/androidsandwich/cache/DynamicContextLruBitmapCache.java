package org.bitbucket.fredgrott.androidsandwich.cache;

import android.graphics.Bitmap;



import android.support.v4.util.LruCache;

public class DynamicContextLruBitmapCache {
	LruCache<ContextAndKey<String>, Bitmap> cache;

	public DynamicContextLruBitmapCache(int maxSize) {
		cache = new LruCache<ContextAndKey<String>, Bitmap>(maxSize);
	}
	
	public Bitmap getDrawable(ContextAndKey<String> string){
		return cache.get(string);
	}
	
	public void putDrawable(ContextAndKey<String> string, Bitmap bitmap){
		cache.put(string, bitmap);
		
	}
	
	
	public void clean(){
		cache.evictAll();
	}
}
