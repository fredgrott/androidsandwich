package org.bitbucket.fredgrott.androidsandwich.cache;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;

public class ConstantContextLruDrawableCache {

	ConstantContextLruCache<String, Drawable> cache;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ConstantContextLruDrawableCache(int maxSize, Context context) {
		cache = new ConstantContextLruCache(maxSize, context);
	}
	
	@SuppressWarnings("unchecked")
	public Drawable getDrawable(String string){
		return (Drawable) cache.get(string);
	}
	
	@SuppressWarnings("unchecked")
	public void putDrawable(String string, Bitmap bitmap){
		cache.put(string,  bitmap);
	}
	
	public void clean(){
		cache.evictAll();
	}
}
