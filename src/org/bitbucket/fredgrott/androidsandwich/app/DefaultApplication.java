package org.bitbucket.fredgrott.androidsandwich.app;


import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicInteger;

import org.bitbucket.fredgrott.androidsandwich.utils.StrictModeWrapper;





import android.app.Application;


import android.content.Intent;
import android.content.pm.ApplicationInfo;


public class DefaultApplication extends Application {
	private static boolean strictModeAvailable;
	 
	 
	 private static DefaultApplication instance = null;
	 
	// use the StrictModeWrapper to see if we are running on Android 2.3 or higher and StrictMode is available
	    static {
	        try {
	            StrictModeWrapper.checkAvailable();
	            strictModeAvailable = true;
	        } catch (Throwable throwable) {
	            strictModeAvailable = false;
	        }
	    }
	 
	 

     public static interface OnLowMemoryListener {
        
        /**
         * Callback to be invoked when the system needs memory.
         */
        public void onLowMemoryReceived();
    }
     
     
     
     
     public static DefaultApplication getInstance(){
    	 checkInstance();
         return instance;
     }
     
     
     private static void checkInstance() {
         if (instance == null)
             throw new IllegalStateException("Application not created yet!");
     }
     
     
     private static final int CORE_POOL_SIZE = 5;

     private static final ThreadFactory sThreadFactory = new ThreadFactory() {
         private final AtomicInteger mCount = new AtomicInteger(1);

         public Thread newThread(Runnable r) {
             return new Thread(r, "Application thread #" + mCount.getAndIncrement());
         }
     };
     
     private ExecutorService mExecutorService;
     // need an abstract class that configures which LrucCahe we want to use
     
     // set up Array for our low memory listener
     private ArrayList<WeakReference<OnLowMemoryListener>> mLowMemoryListeners;
     
     /**
      * @hide
      */
     public DefaultApplication(){
    	 
    	 
    	 mLowMemoryListeners = new ArrayList<WeakReference<OnLowMemoryListener>>();
     }
     
     public ExecutorService getExecutor() {
         if (mExecutorService == null) {
             mExecutorService = Executors.newFixedThreadPool(CORE_POOL_SIZE, sThreadFactory);
         }
         return mExecutorService;
     }
     
     public Class<?> getHomeActivityClass() {
         return null;
     }

     public Intent getMainApplicationIntent() {
         return null;
     }
     
     public void registerOnLowMemoryListener(OnLowMemoryListener listener) {
         if (listener != null) {
             mLowMemoryListeners.add(new WeakReference<OnLowMemoryListener>(listener));
         }
     }
     
     public void unregisterOnLowMemoryListener(OnLowMemoryListener listener) {
         if (listener != null) {
             int i = 0;
             while (i < mLowMemoryListeners.size()) {
                 final OnLowMemoryListener l = mLowMemoryListeners.get(i).get();
                 if (l == null || l == listener) {
                     mLowMemoryListeners.remove(i);
                 } else {
                     i++;
                 }
             }
         }
     }

     @Override
     public void onLowMemory() {
         super.onLowMemory();
         int i = 0;
         while (i < mLowMemoryListeners.size()) {
             final OnLowMemoryListener listener = mLowMemoryListeners.get(i).get();
             if (listener == null) {
                 mLowMemoryListeners.remove(i);
             } else {
                 listener.onLowMemoryReceived();
                 i++;
             }
         }
     }
     
    @Override
    public void onCreate() {
    	//API 9 or greater allows strict mode to
    	// help in debugging
    	if (strictModeAvailable) {
            // check if android:debuggable is set to true
            int applicationFlags = getApplicationInfo().flags;
            if ((applicationFlags & ApplicationInfo.FLAG_DEBUGGABLE) != 0) {
                StrictModeWrapper.enableDefaults();
            }
        }
    	
    	super.onCreate();
    	//provide an instance for our static accessors
        instance = this;
        
        
        // setup/initialize our app cache stuff here
        
        setUpCaches();
        
        setUpSharedPrefs();
    }
    
    /**
     * The idea is if the dev wants caches set up they 
     * extend DefaultApplication class and over-ride setUpCaches
     */
	public void setUpCaches(){
    	
    	
    }
	
	
	
    public void setUpSharedPrefs(){
    	
    }

}
