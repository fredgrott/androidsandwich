package org.bitbucket.fredgrott.androidsandwich.utils;



import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

// TODO: Auto-generated Javadoc
/**
 * By extending this class you get the power of 
 * defining your own global vars/preferences to have 
 * stored.
 * 
 * The call in the application class 
 * 
 * @author fredgrott
 *
 */
public class AppPreferences {

	
	/** The shared prefs tag. */
	public static String sharedPrefsTag;
	
	/** The app shared prefs. */
	private SharedPreferences appSharedPrefs;
    
    /** The prefs editor. */
    @SuppressWarnings("unused")
	private Editor prefsEditor;

    /**
     * Instantiates a new app preferences.
     *
     * @param context the context
     */
    public AppPreferences(Context context)
    {
        this.appSharedPrefs = context.getSharedPreferences(getSharedPrefsTag(), Activity.MODE_PRIVATE);
        this.prefsEditor = appSharedPrefs.edit();
    }
    
    /**
     * You will lover-ride this set your own tag.
     *
     * @param prefstag the new shared prefs tag
     */
    public  void setSharedPrefsTag(String prefstag) {
		sharedPrefsTag = prefstag;
    	
    }
    
    /**
     * Gets the shared prefs tag.
     *
     * @return the shared prefs tag
     */
    public static String getSharedPrefsTag() {
    	return sharedPrefsTag;
    }
    
}
