package org.bitbucket.fredgrott.androidsandwich.utils;

import java.io.File;

import org.bitbucket.fredgrott.androidsandwich.app.DefaultApplication;


import android.content.pm.ApplicationInfo;

import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Environment;

/**
 * AppConstants   if var not set to non-null than we do both setter and getter methods,
 * otherwise we eschew using those as a performance improvement
 * @author fredgrott
 *
 */
public class AppConstants {
	
	public static File externalCacheDirectory = DefaultApplication.getInstance().getExternalCacheDir();
	public static File cacheDirectory = DefaultApplication.getInstance().getCacheDir();
	public static File sdCardLocation =  Environment.getExternalStorageDirectory();
	public static String packageName = DefaultApplication.getInstance().getPackageName();
	public static PackageManager pm = DefaultApplication.getInstance().getPackageManager();
	public static Drawable appIcon = getApplicationInfo().loadIcon(pm);
	public static Drawable appLogo = getApplicationInfo().loadLogo(pm);
	public static String appDataDir = getApplicationInfo().dataDir;
	public static CharSequence appDescription = getApplicationInfo().loadDescription(pm);
	public static int appTargetSDK = getApplicationInfo().targetSdkVersion;
	
	
	String appStringTag = packageName;
	
	/**
	 * Build.VERSION.SDK string depreciated since target 4
	 * thus we use Build.VERSION.SDK_INT
	 * 
	 */
	public static int appSDKVersion=Build.VERSION.SDK_INT;
	
	public static int getAppSDKVersion() {
		return appSDKVersion;
	}
	
	public static ApplicationInfo getApplicationInfo(){
		ApplicationInfo info = null;
		try {
			info = DefaultApplication.getInstance().getPackageManager().getApplicationInfo(packageName, 0);
		} catch(NameNotFoundException e){
			
		}
		return info;
	}
	
	public static int getAppVersion() {
		int appVersion;
		try {
			 appVersion = DefaultApplication.getInstance().getPackageManager().getPackageInfo(packageName, 0).versionCode;
		} catch(NameNotFoundException e){
			 appVersion = 1;
			
		}
		return appVersion;
	}
	
	
	

}
