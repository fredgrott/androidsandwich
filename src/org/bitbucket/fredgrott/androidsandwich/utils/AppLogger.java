package org.bitbucket.fredgrott.androidsandwich.utils;

/**
 * We are not calling androoid.util.Log methods 
 * directly as we use a stack trace set of methods to get
 * the full details of our log stuff rather than just a single line of
 * fluff.
 * 
 * 
 * @author fredgrott
 *
 */
public class AppLogger {
	
	
	public static void i(String message) {
        LogUtils.callLogger("i", LogUtils.getDEFAULTTAG(), message);
 }

 public static void i(String message, String tag) {
        LogUtils.callLogger("i", tag, message);
 }

 public static void d(String message) {
        LogUtils.callLogger("d", LogUtils.getDEFAULTTAG(), message);
 }

 public static void d(String message, String tag) {
        LogUtils.callLogger("d", tag, message);
 }

 public static void e(String message) {
        LogUtils.callLogger("e", LogUtils.getDEFAULTTAG(), message);
 }

 public static void e(String message, String tag) {
        LogUtils.callLogger("e", tag, message);
 }

 public static void w(String message) {
        LogUtils.callLogger("w", LogUtils.getDEFAULTTAG(), message);
 }

 public static void w(String message, String tag) {
        LogUtils.callLogger("w", tag, message);
 }

 public static void v(String message) {
        LogUtils.callLogger("v", LogUtils.getDEFAULTTAG(), message);
 }

 public static void v(String message, String tag) {
        LogUtils.callLogger("v", tag, message);
 }


}
