package org.bitbucket.fredgrott.androidsandwich.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Locale;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.provider.Settings;

import android.provider.Settings.SettingNotFoundException;

public class AppDiagnostics {

	/**
	 * need to initialize,as cannot have blank filed(java5)
	 */
	public static int  androidAPILevel = -1;
	
	public static final int  codenameJELLYBEAN = 16;
	public static final int codenameICS = 14;
	public static final int codenameHONEYCOMB = 11;
	public static final int codenameGINGERBREAD =  9;
	public static final int codenameFROYO = 8;
	public static final int codenameECLAIR = 7;
	public static final int codenameDONUT =4;
	
	public static boolean SUPPORTS_ICS= false;
    public static boolean SUPPORTS_JELLYBEAN = false;
	public static boolean SUPPORTS_HONEYCOMB = false;
	public static boolean SUPPORTS_GINGERBREAD = false;
	public static boolean SUPPORTS_FROYO = false;
	public static boolean SUPPORTS_ECLAIR = false;
	public static boolean SUPPORTS_DONUT = false;
	
	private static boolean test = false;
	private static int testAndroidApiLevel;
	
	static {
		
		 androidAPILevel = AppConstants.getAppSDKVersion();
		 /**
		  * we can just use the result of a comparing two ints which will also be a 
		  * boolean result to set the supports vars, that way we get rid of unnecessary if statements
		  * and thus gives us a performance tweak.  You see this as one of many examples of
		  * using java knowledge to get a performance boost in mobile java and its not the only one.
		  */
		 SUPPORTS_ICS = androidAPILevel >= codenameICS;
	        SUPPORTS_HONEYCOMB = androidAPILevel >= codenameHONEYCOMB;
	        SUPPORTS_GINGERBREAD = androidAPILevel >= codenameGINGERBREAD;
	        SUPPORTS_FROYO = androidAPILevel >= codenameFROYO;
	        SUPPORTS_ECLAIR = androidAPILevel >= codenameECLAIR;
	        SUPPORTS_DONUT = androidAPILevel >= codenameDONUT;
	        SUPPORTS_JELLYBEAN = androidAPILevel >= codenameJELLYBEAN;
	}
	
	public static boolean isTest() {
        return test;
    }
	
	public static void setTestAndroidApiLevel(int myAndroidApiLevel) {
		testAndroidApiLevel = myAndroidApiLevel;
		 test = true;
	}
	
	public static boolean supportsApiLevel(int apiLevel) {
        if (test) {
            return testAndroidApiLevel >= apiLevel;
        } else {
            return androidAPILevel >= apiLevel;
        }
    }
	
	/**
	 * UUID is the only number non null across all use cases no
	 * matter if the device is non-google-exp or not, tablet, phone, or googletv, 
	 * and has the benefits of being the same across factory resets while 
	 * ANDROID_ID does not.
	 * 
	 * We randomize it within AndoridDeviceID class to prevent some things.
	 *          
	 * @param context
	 * @return
	 */
	public static String getAndroidId() {
        
        return AndroidDeviceID.getUuid().toString();
    }

	public static String getApplicationVersionString(Context context) {
        try {
            PackageManager pm = context.getPackageManager();
            PackageInfo info = pm.getPackageInfo(context.getPackageName(), 0);
            return "v" + info.versionName;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
	
	public static String createDiagnosis(Activity context, Exception error) {
		StringBuilder sb = new StringBuilder();
		
		sb.append("Application version: " + getApplicationVersionString(context) + "\n");
        sb.append("Device locale: " + Locale.getDefault().toString() + "\n\n");
        sb.append("Android ID: " + getAndroidId());

        // phone information
        sb.append("PHONE SPECS\n");
        sb.append("model: " + Build.MODEL + "\n");
        sb.append("brand: " + Build.BRAND + "\n");
        sb.append("product: " + Build.PRODUCT + "\n");
        sb.append("device: " + Build.DEVICE + "\n\n");

        // android information
        sb.append("PLATFORM INFO\n");
        sb.append("Android " + Build.VERSION.RELEASE + " " + Build.ID + " (build "
                + Build.VERSION.INCREMENTAL + ")\n");
        sb.append("build tags: " + Build.TAGS + "\n");
        sb.append("build type: " + Build.TYPE + "\n\n");

        // settings
        sb.append("SYSTEM SETTINGS\n");
        String networkMode = null;
        ContentResolver resolver = context.getContentResolver();
        try {
            if (Settings.Secure.getInt(resolver, Settings.Secure.WIFI_ON) == 0) {
                networkMode = "DATA";
            } else {
                networkMode = "WIFI";
            }
            sb.append("network mode: " + networkMode + "\n");
            sb.append("HTTP proxy: "
                    + Settings.Secure.getString(resolver, Settings.Secure.HTTP_PROXY) + "\n\n");
        } catch (SettingNotFoundException e) {
            e.printStackTrace();
        }

        sb.append("STACK TRACE FOLLOWS\n\n");

        StringWriter stackTrace = new StringWriter();
        error.printStackTrace(new PrintWriter(stackTrace));

        sb.append(stackTrace.toString());

        return sb.toString();
    }
}
	
	

