package org.bitbucket.fredgrott.androidsandwich.utils;

import java.util.UUID;

import android.content.Context;
import android.content.SharedPreferences;


// TODO: Auto-generated Javadoc
/**
 * Okay, we have some use cases:
 * 
 *        -During factory reset(google exp devices) we might get:
 *                                                           our DeviceID                             APPID
 *         ANDROID_ID               different                              different
 *         UUID                               same                                         same
 *         deviceID                           same                                        same
 *      -Non-google exp devices(even for factory resets)
 *         ANDROID_ID                 null                                              null
 *          UUID                                same                                           same
 *          deviceID                         same                                           same
 *      -Non-telephone devices
 *           ANDROID_ID               null or string                            null or string
 *           UUID                                  maybe                                    maybe
 *           deviceID                            Null?                                         null?
 *           
 *     Factory-rest is the out-liner that we will not be ale to prevent as far as having
 *     a different ourDeviceID number or different APPID number.  With non-google-exp 
 *     devices, googleTV, tablets,phones, watches, etc we will always have an UUID number and
 *     thus its probably better to rely on that long as we randomize the number and use that to
 *     store for the appID install number. Which means I proved Michael Burt of Groupon wrong, sweet!
 *     
 *     Thus, we have:
 *     
 *     -if read from preferences determines that uuid was not stored getUUID.randomizeIT
 *     -store randomizedUUID in preferences to use as devicdID and appID.
 *     
 *     Why random instead of say hashCode? Remember, ANDROID has multiple DVM instances
 *     and hashCode will not be unique across DVM instances. Whereas randomizing the UUID will be unique
 *     across DVMs.
 *     
 *     
 *     Credits:
 *     Michael Burt(Lead Android Dev at Groupon) getting it part right: 
 *         web url: http://stackoverflow.com/questions/2785485/is-there-a-unique-android-device-id
 *     Carlos P getting it part improving MB's answer:
 *         web url: http://stackoverflow.com/questions/2322234/how-to-find-serial-number-of-android-device
 *           
 *      -
 * @author fredgrott
 *
 */
public class AndroidDeviceID {

	/** The Constant PREFS_FILE. */
	protected static final String PREFS_FILE = "device_id.xml";
    
    /** The Constant PREFS_DEVICE_ID. */
    protected static final String PREFS_DEVICE_ID = "device_id";

    /** The uuid. */
    protected static UUID uuid;

    /**
     * Instantiates a new android device id.
     *
     * @param context the context
     */
    public AndroidDeviceID(Context context) {
		
    	if(uuid == null) {
    		synchronized (AndroidDeviceID.class) {
    			/**
    			 * if uuid is now null than we should read in the preferences to see if they
    			 * were set.
    			 */
    			if(uuid == null) {
    				final SharedPreferences prefs = context.getSharedPreferences(PREFS_FILE, 0);
                    final String id = prefs.getString(PREFS_DEVICE_ID, null);
                    
                    if (id != null) {
                        // Use the ids previously computed and stored in the prefs file
                        uuid = UUID.fromString(id);

                    } else {
                    	uuid = UUID.randomUUID();
                    	 // Write the value out to the prefs file so it persists
                        prefs.edit().putString(PREFS_DEVICE_ID, uuid.toString()).commit();
                    }
    			}
    		}
    	}
	}
	
	/**
	 * Gets the uuid.
	 *
	 * @return the uuid
	 */
	public static UUID getUuid() {
		return uuid;
	}
}
