package org.bitbucket.fredgrott.androidsandwich.utils;

import java.io.File;



import android.os.Environment;
import android.os.StatFs;

public class MemoryUtils {
	
	static final int ERROR = -1;
	
	/**
	 * We have several devices that do not have
	 * SDCARD slots so we check to see if media is
	 * mounted before making any call to 
	 * determine externalMedia memory size
	 * @return
	 */
	public static boolean isMounted(){
   	 if (android.os.Environment.getExternalStorageState().equals(
   		        android.os.Environment.MEDIA_MOUNTED)) {
   		      return true;
   		    }
   		    return false;
    }
	
	public static boolean isCacheExternalizable() {
		if(isMounted() | AppConstants.externalCacheDirectory != null){
			return true;
		} else {
			return false;
		}
	}
	
	public static boolean isCacheSaveableExtenal() {
		if(isMounted() | AppConstants.sdCardLocation != null){
			return true;
		} else{
			return false;
		}
	}
	 public static boolean isCacheSaveableInternal() {
		 if(AppConstants.cacheDirectory != null){
			 return true;
		 } else {
			 return false;
		 }
	 }
	
	static public long getAvailableInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long availableBlocks = stat.getAvailableBlocks();
        return availableBlocks * blockSize;
    }

    static public long getTotalInternalMemorySize() {
        File path = Environment.getDataDirectory();
        StatFs stat = new StatFs(path.getPath());
        long blockSize = stat.getBlockSize();
        long totalBlocks = stat.getBlockCount();
        return totalBlocks * blockSize;
     }
    
    static public long getAvailableExternalMemorySize() {
        if(isMounted()) {
                File path = Environment.getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSize();
                long availableBlocks = stat.getAvailableBlocks();
                return availableBlocks * blockSize;
        } else {
                return ERROR;
        }
}

     static public long getTotalExternalMemorySize() {
        if(isMounted()) {
        	    File path = Environment.getExternalStorageDirectory();
                StatFs stat = new StatFs(path.getPath());
                long blockSize = stat.getBlockSize();
                long totalBlocks = stat.getBlockCount();
                return totalBlocks * blockSize;
        } else {
                return ERROR;
        }
}
    
    static public String formatSize(long size) {
        String suffix = null;

        if (size >= 1024) {
                suffix = "KiB";
                size /= 1024;
                if (size >= 1024) {
                        suffix = "MiB";
                        size /= 1024;
                }
                if (size >= 1024) {
                	suffix = "GiB";
                	size /= 1024;
                }
        }

        StringBuilder resultBuffer = new StringBuilder(Long.toString(size));

        int commaOffset = resultBuffer.length() - 3;
        while (commaOffset > 0) {
                resultBuffer.insert(commaOffset, ',');
                commaOffset -= 3;
        }

        if (suffix != null)
                resultBuffer.append(suffix);
        return resultBuffer.toString();
    }

}
