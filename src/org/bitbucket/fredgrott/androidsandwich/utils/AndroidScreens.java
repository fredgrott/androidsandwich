package org.bitbucket.fredgrott.androidsandwich.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.util.DisplayMetrics;

// TODO: Auto-generated Javadoc
/**
 *   Borrowed from Kaeppler's Ignition project, modified as we no longer target 1.5 
 *   and thus all the fields we access are public so no need to use reflection to get
 *   access to those fields.
 *   
 * @author fredgrott
 *
 */
public class AndroidScreens {

	/** The Constant SCREEN_DENSITY_LOW. */
	public static final int SCREEN_DENSITY_LOW = 120;
    
    /** The Constant SCREEN_DENSITY_MEDIUM. */
    public static final int SCREEN_DENSITY_MEDIUM = 160;
    
    /** The Constant SCREEN_DENSITY_HIGH. */
    public static final int SCREEN_DENSITY_HIGH = 240;
    
    /** The Constant SCREEN_DENSITY_XHIGH. */
    public static final int SCREEN_DENSITY_XHIGH = 320;
    
    /** The Constant SCREEN_DENSITY_TV. */
    public static final int SCREEN_DENSITY_TV = 213;
    
    /** The Constant SCREEN_SIZE_QVGA. */
    public static final String SCREEN_SIZE_QVGA = "240x320";
    
    /** The Constant SCREEN_SIZE_WQVGA400. */
    public static final String SCREEN_SIZE_WQVGA400 = "240x400";
    
    /** The Constant SCREEN_SIZE_WQVGA432. */
    public static final String SCREEN_SIZE_WQVGA432 = "240x432";
    
    /** The Constant SCREEN_SIZE_HVGA. */
    public static final String SCREEN_SIZE_HVGA = "320x480";
    
    /** The Constant SCREEN_SIZE_WVGA800. */
    public static final String SCREEN_SIZE_WVGA800 = "480x800";
    
    /** The Constant SCREEN_SIZE_WVGA854. */
    public static final String SCREEN_SIZE_WVGA854 = "480x854";
    
    /** The Constant SCREEN_SIZE_800_600. */
    public static final String SCREEN_SIZE_800_600 = "800x600";
    
    /** The Constant SCREEN_SIZE_600_800. */
    public static final String SCREEN_SIZE_600_800 = "600x800";
    
    /** The Constant SCREEN_SIZE_800_480. */
    public static final String SCREEN_SIZE_800_480 = "800x480";
    
    /** The Constant SCREEN_SIZE_854_480. */
    public static final String SCREEN_SIZE_854_480 = "854x480";
    
    /** The screen size type. */
    private static String screenSizeType = "";
    
    /** The screen size w. */
    private static int screenSizeW = -1;
    
    /** The screen size h. */
    private static int screenSizeH = -1;
    
    /** The screen density. */
    private static int screenDensity = -1;
    
    /**
     * Dip to px.
     *
     * @param context the context
     * @param dip the dip
     * @return the int
     */
    public static int dipToPx(Context context, int dip) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) (dip * displayMetrics.density + 0.5f);
    }

    /**
     * Scale drawable.
     *
     * @param context the context
     * @param drawableResourceId the drawable resource id
     * @param width the width
     * @param height the height
     * @return the drawable
     */
    public static Drawable scaleDrawable(Context context, int drawableResourceId, int width,
            int height) {
        Bitmap sourceBitmap = BitmapFactory.decodeResource(context.getResources(),
            drawableResourceId);
        return new BitmapDrawable(Bitmap.createScaledBitmap(sourceBitmap, width, height, true));
    }
    
    /**
     * Gets the screen density.
     *
     * @param context the context
     * @return the screen density
     */
    public  int getScreenDensity(Context context) {
        if (screenDensity == -1) {
            DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
            
            screenDensity = displayMetrics.densityDpi;
            
        }
        return screenDensity;
    }
    
/**
 * Gets the screen size type.
 *
 * @param context the context
 * @return the screen size type
 */
public static String getScreenSizeType(Context context) {// returns type
        
    	if (TextUtils.isEmpty(screenSizeType)) {
	    	DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
	    	if (screenSizeW == -1) {
	    		
	    		screenSizeW = displayMetrics.widthPixels;
	    		
	        }
	    	if (screenSizeH == -1) {
	    		
	    		screenSizeH = displayMetrics.heightPixels;
	    		
	           
	        }
    	}
    	
    	String scrSize = screenSizeW + "x" + screenSizeH;
    	if (scrSize.equals(SCREEN_SIZE_QVGA)) {
			screenSizeType = SCREEN_SIZE_QVGA;
		} else if (scrSize.equals(SCREEN_SIZE_WQVGA400)) {
			screenSizeType = SCREEN_SIZE_WQVGA400;
		} else if (scrSize.equals(SCREEN_SIZE_WQVGA432)) {
			screenSizeType = SCREEN_SIZE_WQVGA432;
		} else if (scrSize.equals(SCREEN_SIZE_WVGA800)) {
			screenSizeType = SCREEN_SIZE_WVGA800;
		} else if (scrSize.equals(SCREEN_SIZE_WVGA854)) {
			screenSizeType = SCREEN_SIZE_WVGA854;
		} else if (scrSize.equals(SCREEN_SIZE_800_600)) {
			screenSizeType = SCREEN_SIZE_800_600;
		} else if (scrSize.equals(SCREEN_SIZE_600_800)) {
			screenSizeType = SCREEN_SIZE_600_800;
		} else if (scrSize.equals(SCREEN_SIZE_800_480)) {
			screenSizeType = SCREEN_SIZE_800_480;
		} else if (scrSize.equals(SCREEN_SIZE_854_480)) {
			screenSizeType = SCREEN_SIZE_854_480;
		} else {
			screenSizeType = SCREEN_SIZE_HVGA;
		}
    	
        return screenSizeType;
    }
    
   /**
    * Gets the screen size w.
    *
    * @param context the context
    * @return the screen size w
    */
   public static int getScreenSizeW(Context context) {
         if (screenSizeW == -1) {
              DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        
              screenSizeW = displayMetrics.widthPixels;
        
        
            }
        return screenSizeW;
     }

   
   /**
    * Gets the screen size h.
    *
    * @param context the context
    * @return the screen size h
    */
   public static int getScreenSizeH(Context context) {
       if (screenSizeH == -1) {
           DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
           
           screenSizeH = displayMetrics.heightPixels;
           
          
       }
       return screenSizeH;
   }
   
   // temporary func to check xlarge screen
   /** The Constant SCREENLAYOUT_SIZE_XLARGE. */
   private static final int SCREENLAYOUT_SIZE_XLARGE = 4;
   
   /**
    * Checks if is screen x large.
    *
    * @param context the context
    * @return true, if is screen x large
    */
   public static final boolean isScreenXLarge(Context context) {
       int screenLayout = context.getResources().getConfiguration().screenLayout;
       int size = screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK;
       return size == SCREENLAYOUT_SIZE_XLARGE;
   }
}
